#include "stdafx.h"
#include "FMReview.h"
#include "FHRAutodiagnostics.h"

FHR_AutodiagnosticsFunction CFHRAutodiagnostics::m_fnFHRAutodiagno = NULL;
CCriticalSection CFHRAutodiagnostics::m_csDiagnoLock;

CFHRAutodiagnostics::CFHRAutodiagnostics(void)
{
}

CFHRAutodiagnostics::~CFHRAutodiagnostics(void)
{
}

BOOL CFHRAutodiagnostics::InitModule(CString sDLLPath)
{
	HINSTANCE hDLL = ::LoadLibrary(sDLLPath);
	if (! hDLL) {
		CString err;
		err.Format(_T("CFHRAutodiagnostics::InitModule失败，错误代码：%d\n"), GetLastError());
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		return FALSE;
	}

	m_fnFHRAutodiagno = (FHR_AutodiagnosticsFunction)GetProcAddress(hDLL, "zidongzhenduan");
	if (! m_fnFHRAutodiagno) {
		FreeLibrary(hDLL);
		CString err = _T("CFHRAutodiagnostics::InitModule失败，GetProcAddress失败\n");
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		return FALSE;
	}
	
	return TRUE;
}

void CFHRAutodiagnostics::SetBuffer(BYTE* fhr_buf, BYTE* toco_buf, short buf_len)
{
	if (! theApp.HasAutoDiagno()) {
		return;
	}
	ZeroMemory(m_fhr_buf, sizeof(m_fhr_buf));
	ZeroMemory(m_toco_buf, sizeof(m_toco_buf));
	memcpy(m_fhr_buf, fhr_buf, sizeof(BYTE) * min(buf_len, FHR_AUTODIAGNO_INPUT_LEN));
	memcpy(m_toco_buf, toco_buf, sizeof(BYTE) * min(buf_len, FHR_AUTODIAGNO_INPUT_LEN));
}

BOOL CFHRAutodiagnostics::StartAutodiagno(void)
{
	if (! theApp.HasAutoDiagno()) {
		return TRUE;
	}
	CSmartLock lock(m_csDiagnoLock);
	m_bSuccess = FALSE;
	try {
		ZeroMemory(m_autodiagnostics_result, sizeof(m_autodiagnostics_result));
		m_bSuccess = m_fnFHRAutodiagno(m_fhr_buf, m_toco_buf, m_autodiagnostics_result, FHR_AUTODIAGNO_INPUT_LEN);
	}
	catch (CNotSupportedException ex) {
		m_bSuccess = FALSE;
	}
	if (m_bSuccess) {
		ParseResult();
	}
	return m_bSuccess;
}

void CFHRAutodiagnostics::ParseResult(void)
{
	if (! theApp.HasAutoDiagno()) {
		return;
	}
	m_nFlagBufLen = 0;
	int i_flag = 0;
	ZeroMemory(&m_fischer, sizeof(m_fischer));
	ZeroMemory(m_flag_buf, sizeof(m_flag_buf));
	BYTE* buf = m_autodiagnostics_result;

	int i = 0;
	while (i < FHR_AUTODIAGNO_RESULT_LEN) {
		if (0x0055 == buf[i] &&
			0x00AA == buf[i+1] &&
			0x0002 == buf[i+2]) {
			//解析加速信息
			i += 3;
			ParseAcc(buf, &i, m_flag_buf, &i_flag);
		}
		else if (0x0055 == buf[i] &&
			0x00AA == buf[i+1] &&
			0x0004 == buf[i+2]) {
			//解析减速信息
			i += 3;
			ParseDec(buf, &i, m_flag_buf, &i_flag);
		}
		else if (0x0055 == buf[i] &&
			0x00AA == buf[i+1] &&
			0x0005 == buf[i+2]) {
			//fischer信息总是位于尾部
			i += 3;
			memcpy(&m_fischer, &(buf[i]), sizeof(m_fischer));
			i += sizeof(m_fischer);
		}
		else {
			//出错了，加一试试
			i ++;
		}
	}

	m_nFlagBufLen = (i_flag + 1);
}

void CFHRAutodiagnostics::ParseAcc(BYTE* buf, int* pi, DIAGNOSIS_FLAG* pFlagBuf, int* pi_flag)
{
	if (! theApp.HasAutoDiagno()) {
		return;
	}
	while (*pi < FHR_AUTODIAGNO_RESULT_LEN) {
		short hi = buf[*pi];
		short lo = buf[(*pi)+1];
		short type = buf[(*pi)+2];
		if (0x0055 == hi && 0x00AA == lo) {
			return;
		}
		int index = (int)MAKEWORD(LOBYTE(lo),LOBYTE(hi));
		pFlagBuf[*pi_flag].nIndex = index;
		pFlagBuf[*pi_flag].nY = (int)m_fhr_buf[index];
		pFlagBuf[*pi_flag].nType = (int)type;
		(*pi_flag) ++;
		(*pi) += 3;
	}
}

void CFHRAutodiagnostics::ParseDec(BYTE* buf, int* pi, DIAGNOSIS_FLAG* pFlagBuf, int* pi_flag)
{
	if (! theApp.HasAutoDiagno()) {
		return;
	}
	while (*pi < FHR_AUTODIAGNO_RESULT_LEN) {
		short hi = buf[*pi];
		short lo = buf[(*pi)+1];
		short type = buf[(*pi)+2];
		if (0x0055 == hi && 0x00AA == lo) {
			return;
		}
		int index = (int)MAKEWORD(LOBYTE(lo),LOBYTE(hi));
		pFlagBuf[*pi_flag].nIndex = index;
		pFlagBuf[*pi_flag].nY = (int)m_fhr_buf[index];
		pFlagBuf[*pi_flag].nType = - (int)type;
		(*pi_flag) ++;
		(*pi) += 3;
	}
}

DIAGNOSIS_FLAG* CFHRAutodiagnostics::FindFlag(int indexStart, int indexCount)
{
	if (! theApp.HasAutoDiagno()) {
		return NULL;
	}
	int i;
	for (i=0; i<m_nFlagBufLen; i++) {
		DIAGNOSIS_FLAG* pf = &(m_flag_buf[i]);
		if (pf->nIndex >= indexStart &&
			pf->nIndex < (indexStart + indexCount)) {
			return pf;
		}
	}
	return NULL;
}
