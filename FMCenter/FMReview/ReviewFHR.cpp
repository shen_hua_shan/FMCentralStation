#include "stdafx.h"
#include "ReviewFHR.h"
#include "ReviewDlg.h"
#include "FHRAutodiagnostics.h"

static const int top_margin = 5;
static const int grid_gap_y = 20;
static const int grid_gap_x = 60;
static const int fhr_toco_gap = 30;
static const int fhr_scale_x = 360;
static const int text_h = 16;
static const COLORREF cr_fhr1 = RGB(96, 255, 96);
static const COLORREF cr_fhr2 = RGB(255, 128, 96);
static const COLORREF cr_fhr1_flag = RGB(192, 255, 225);
static const COLORREF cr_fhr2_flag = RGB(255, 192, 225);
static const COLORREF cr_fhr_normal = RGB(24, 36, 0);
static const COLORREF cr_toco = RGB(225, 225, 128);
static const COLORREF cr_mark = RGB(255, 0, 0);
static const COLORREF cr_time_mark = RGB(0, 192, 255);

CReviewFHR::CReviewFHR(CReviewDlg* pdlg)
	: CReviewTool(pdlg)
{
	m_pAuto1 = new CFHRAutodiagnostics();
	m_pAuto2 = new CFHRAutodiagnostics();
	m_fontTimeScale.CreateFont(16, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_fontTableScale.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
	m_pOldDataPtr = NULL;
}

CReviewFHR::~CReviewFHR(void)
{
	m_pOldFont = NULL;
	if (m_pAuto1) {
		delete m_pAuto1;
		m_pAuto1 = NULL;
	}
	if (m_pAuto2) {
		delete m_pAuto2;
		m_pAuto2 = NULL;
	}
}

void CReviewFHR::SelectTimeRulerFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontTimeScale);
}

void CReviewFHR::SelectTableScaleFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontTableScale);
}

void CReviewFHR::ResetFont(CDC* pdc)
{
	if (m_pOldFont) {
		pdc->SelectObject(m_pOldFont);
	}
}

int CReviewFHR::GetPageSpan()
{
	CRect rc = m_pReviewDlg->GetTableRect();
	return (rc.Width() / 2);
}

void CReviewFHR::EnterReview()
{
	m_pReviewDlg->m_cmbSelectChannel.ResetContent();
	m_pReviewDlg->m_cmbSelectChannel.AddString(_T("FHR 1"));
	m_pReviewDlg->m_cmbSelectChannel.AddString(_T("FHR 2"));
	m_pReviewDlg->m_cmbSelectChannel.SetCurSel(0);
	m_pReviewDlg->m_cmbSelectChannel.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_cmbSelectFormat.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_staSelectFormat.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_staDoctorDiagno.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_cmbDoctorDiagno.ShowWindow(SW_SHOW);
}

void CReviewFHR::LeaveReview()
{
	m_pReviewDlg->m_cmbSelectChannel.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_cmbSelectFormat.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_staSelectFormat.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_staDoctorDiagno.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_cmbDoctorDiagno.ShowWindow(SW_HIDE);
}

void CReviewFHR::Draw(CDC* pdc)
{
	m_pReviewDlg->InitBkgd(pdc, RGB(0,0,0));

	CRect rc = m_pReviewDlg->GetTableRect();
	rc.OffsetRect(-rc.left, -rc.top);

	DrawGrid(pdc, rc);
	DrawTime(pdc, rc);
	DrawWave(pdc, rc);
}

void CReviewFHR::DrawGrid(CDC* pdc, CRect& rc)
{
	CPen grayPen(PS_SOLID, 1, RGB(64, 64, 64));
	CPen redPenS(PS_SOLID, 1, RGB(128, 0, 0));
	LOGBRUSH logBrush;
	logBrush.lbStyle = PS_SOLID;
	logBrush.lbColor = RGB(128, 0, 0);
	DWORD dwF[2] = {4, 4};
	CPen redPenD(PS_USERSTYLE|PS_GEOMETRIC|PS_ENDCAP_ROUND, 1, &logBrush, 2, (LPDWORD)dwF);
	CPen* pOldPen = pdc->SelectObject(&grayPen);

	int x, y, i;
	//正常值区域
	CBrush br(cr_fhr_normal);
	CRect rcNormalArea(rc.left, rc.top + top_margin + grid_gap_y * 8, 
		rc.right, rc.top + top_margin + grid_gap_y * 12);
	pdc->FillRect(&rcNormalArea, &br);

	//FHR实线
	y = rc.top + top_margin;
	for (i=0; i<22; i++) {
		pdc->MoveTo(rc.left, y);
		pdc->LineTo(rc.right, y);
		y += grid_gap_y;
	}
	//TOCO实线
	y += fhr_toco_gap - grid_gap_y;
	for (i=0; i<11; i++) {
		pdc->MoveTo(rc.left, y);
		pdc->LineTo(rc.right, y);
		y += grid_gap_y;
	}
	
	//纵实线
	pdc->SelectObject(&redPenS);
	for (x = rc.left + 1; x<rc.right; x+= grid_gap_x * 2) {
		y = rc.top + top_margin; pdc->MoveTo(x, y);
		y += grid_gap_y * 21;    pdc->LineTo(x, y);
		y += fhr_toco_gap;       pdc->MoveTo(x, y);
		y += grid_gap_y * 10;    pdc->LineTo(x, y);
	}
	//纵虚线
	pdc->SelectObject(&redPenD);
	for (x = rc.left + grid_gap_x + 1; x<rc.right; x+= grid_gap_x * 2) {
		y = rc.top + top_margin; pdc->MoveTo(x, y);
		y += grid_gap_y * 21;    pdc->LineTo(x, y);
		y += fhr_toco_gap;       pdc->MoveTo(x, y);
		y += grid_gap_y * 10;    pdc->LineTo(x, y);
	}

	//标尺
	SelectTableScaleFont(pdc);
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(RGB(225,225,192));
	CString s;
	int scale = 240;
	x = rc.left + 30;
	y = rc.top;
	for (i=0; i<11; i++) {
		s.Format(_T("%3d"), scale);
		pdc->TextOut(x, y, s);
		pdc->TextOut(x + fhr_scale_x, y, s);
		pdc->TextOut(x + fhr_scale_x * 2, y, s);
		pdc->TextOut(x + fhr_scale_x * 3, y, s);
		y += grid_gap_y * 2;
		scale -= 20;
	}
	y -= grid_gap_y;
	scale = 30;
	s.Format(_T("%3d"), scale);
	pdc->TextOut(x, y, s);
	pdc->TextOut(x + fhr_scale_x, y, s);
	pdc->TextOut(x + fhr_scale_x * 2, y, s);
	pdc->TextOut(x + fhr_scale_x * 3, y, s);
	scale = 100;
	y += fhr_toco_gap;
	for (i=0; i<6; i++) {
		s.Format(_T("%3d"), scale);
		pdc->TextOut(x, y, s);
		pdc->TextOut(x + fhr_scale_x, y, s);
		pdc->TextOut(x + fhr_scale_x * 2, y, s);
		pdc->TextOut(x + fhr_scale_x * 3, y, s);
		y += grid_gap_y * 2;
		scale -= 20;
	}

	ResetFont(pdc);
	pdc->SelectObject(pOldPen);
}

void CReviewFHR::DrawTime(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	if (tmTableStart == 0) {
		return;
	}

	SelectTimeRulerFont(pdc);
	int i;
	int nRightLimit = min(nValidLen * 2, rc.Width());
	int nStartOffsetSecond = (60 - tmTableStart.GetSecond()) % 60;//显示到整分钟位置
	for (i=0; TRUE; i++) {
		CTimeSpan ts(0, 0, 0, nStartOffsetSecond + i * 60);
		CTime tmMark = tmTableStart + ts;
		int x = rc.left + (i * 60 + nStartOffsetSecond) * 2;
		if (x > nRightLimit) {
			break;
		}
		int y = rc.top + top_margin + 420;
		DrawTimeMark(pdc, cr_time_mark, tmMark, x + 1, y + 1, fhr_toco_gap - 3);
	}
	ResetFont(pdc);
}

void CReviewFHR::DrawWave(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	SelectTableScaleFont(pdc);
	int nDispCount = min(nValidLen, rc.Width() / 2);
	if (m_pOldDataPtr != pData) {
		m_pOldDataPtr = pData;
		Autodiagnostics(pData, nDispCount);
	}
	
	int i;
	int yFhr1 = rc.top + top_margin;
	int yFhr2 = rc.top + top_margin + 80;
	int yToco = rc.top + top_margin + 420 + fhr_toco_gap;
	BYTE fhr1Data[3];
	BYTE fhr2Data[3];
	BYTE tocoData[3];
	BYTE fmMark;
	for (i=0; i<nDispCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		if (0 == i) {
			fhr1Data[0] = pf->fhr1[1];
			fhr2Data[0] = pf->fhr1[1];
			tocoData[0] = pf->uc[1];
		}
		else {
			fhr1Data[0] = fhr1Data[2];
			fhr2Data[0] = fhr2Data[2];
			tocoData[0] = tocoData[2];
		}
		fhr1Data[1] = pf->fhr1[1];
		fhr2Data[1] = pf->fhr2[1];
		tocoData[1] = pf->uc[1];
		fhr1Data[2] = pf->fhr1[3];
		fhr2Data[2] = pf->fhr2[3];
		tocoData[2] = pf->uc[3];
		fmMark = pf->marked;

		int x = rc.left + 2 * i;

		int j, v1, v2;
		int nBreakGap = 60;
		for (j = 0; j < 2; j ++) {
			//FHR1曲线
			if (0 != fhr1Data[j] && 0 != fhr1Data[j+1]) {
				v1 = 2 * (fhr1Data[j] - 30);
				v2 = 2 * (fhr1Data[j+1] - 30);
				if (abs(v1 - v2) < nBreakGap) {
					//超过30就断线了
					DrawLine(pdc, cr_fhr1, x, 
						yFhr1 + 420 - v1,
						yFhr1 + 420 - v2);
				}
			}

			//FHR2曲线
			if (0 != fhr2Data[j] && 0 != fhr2Data[j+1]) {
				v1 = 2 * (fhr2Data[j] - 30);
				v2 = 2 * (fhr2Data[j+1] - 30);
				if (abs(v1 - v2) < nBreakGap) {
					//超过30就断线了
					DrawLine(pdc, cr_fhr2, x,
						yFhr2 + 420 - v1,
						yFhr2 + 420 - v2);
				}
			}

			//TOCO曲线
			v1 = 2 * tocoData[j];
			v2 = 2 * tocoData[j+1];
			DrawLine(pdc, cr_toco, x,
				yToco + 200 - v1,
				yToco + 200 - v2);

			x ++;
		}

		//绘制胎动标记
		if (fmMark) {
			DrawFMMark(pdc, cr_mark, x, yToco);
		}

		//绘制自动诊断信息
		if (m_pAuto1->Success()) {
			DIAGNOSIS_FLAG* pf = m_pAuto1->FindFlag(i*2, 2);
			if (pf) {
				int flag_y = yFhr1 + 420 - (pf->nY - 30) * 2;
				DrawDiagnosisFlag(pdc, cr_fhr1_flag, x, flag_y, pf->nType);
			}
		}
		if (m_pAuto2->Success()) {
			DIAGNOSIS_FLAG* pf = m_pAuto2->FindFlag(i*2, 2);
			if (pf) {
				int flag_y = yFhr2 + 420 - (pf->nY - 30) * 2;
				DrawDiagnosisFlag(pdc, cr_fhr2_flag, x, flag_y, pf->nType);
			}
		}
	}

	//绘制基线
	if (m_pAuto1->Success()) {
		int fhr1_base = m_pAuto1->GetFhrBaseline();
		if (fhr1_base > 0) {
			CPen pen(PS_SOLID, 1, cr_fhr1_flag);
			CPen* pOldPen = pdc->SelectObject(&pen);
			int y = yFhr1 + 420 - (fhr1_base - 30) * 2;
			pdc->MoveTo(rc.left, y);
			pdc->LineTo(rc.right, y);
			pdc->SelectObject(pOldPen);
		}
	}
	if (m_pAuto2->Success()) {
		int fhr2_base = m_pAuto2->GetFhrBaseline();
		if (fhr2_base > 0) {
			CPen pen(PS_SOLID, 1, cr_fhr2_flag);
			CPen* pOldPen = pdc->SelectObject(&pen);
			int y = yFhr2 + 420 - (fhr2_base - 30) * 2;
			pdc->MoveTo(rc.left, y);
			pdc->LineTo(rc.right, y);
			pdc->SelectObject(pOldPen);
		}
	}

	ResetFont(pdc);
}

void CReviewFHR::DrawDiagnosisFlag(CDC* pdc, COLORREF cr, int x, int y, int type)
{
	if (0 == type) {
		return;
	}
	pdc->SetTextColor(cr);
	if (type > 0) {
		pdc->TextOut(x, y - text_h * 1.5, CFMDict::FHRAutodiagnoAccel_itos(type));
	}
	else {
		pdc->TextOut(x, y + text_h * 0.5, CFMDict::FHRAutodiagnoDecel_itos(-type));
	}
}

void CReviewFHR::Autodiagnostics(CLIENT_DATA* pData, int nCount)
{
	int i;
	nCount = min(nCount, 1200);
	BYTE fhr1_buf[2400];
	BYTE fhr2_buf[2400];
	BYTE toco_buf[2400];
	for (i=0; i<nCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		int nCurIndex = i * 2;
		fhr1_buf[nCurIndex] = pf->fhr1[1];
		fhr1_buf[nCurIndex+1] = pf->fhr1[3];
		fhr2_buf[nCurIndex] = pf->fhr2[1];
		fhr2_buf[nCurIndex+1] = pf->fhr2[3];
		toco_buf[nCurIndex] = pf->uc[1];
		toco_buf[nCurIndex+1] = pf->uc[3];
	}

	m_pAuto1->SetBuffer(fhr1_buf, toco_buf, nCount * 2);
	m_pAuto1->StartAutodiagno();
	m_pAuto2->SetBuffer(fhr2_buf, toco_buf, nCount * 2);
	m_pAuto2->StartAutodiagno();
}