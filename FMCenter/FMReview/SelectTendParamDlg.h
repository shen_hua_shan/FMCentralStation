#pragma once
#include "afxwin.h"


// CSelectTendParamDlg 对话框
class CReviewDlg;
class CSelectTendParamDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSelectTendParamDlg)

public:
	CSelectTendParamDlg(CReviewDlg* pDlg, CRect rc);
	virtual ~CSelectTendParamDlg();

// 对话框数据
	enum { IDD = IDD_SELEDT_TEND_PARAM };

private:
	CReviewDlg* m_pReviewDlg;
	CRect m_rcButton;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CButton m_chkHR;
	CButton m_chkSYS;
	CButton m_chkMEA;
	CButton m_chkDIA;
	CButton m_chkRR;
	CButton m_chkSPO2;
	CButton m_chkT1;
	CButton m_chkT2;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
};
