#include "stdafx.h"
#include "LogFile.h"
#include "FMCenter.h"
#include "FMRecordUnit.h"
#include "FMDisplayDataBuffer.h"


CFMRecordUnit::CFMRecordUnit(int nIndex, HANDLE hEvent)
	: m_nIndex(nIndex)
	, m_db(theApp.m_db)
	, m_dbLock(theApp.m_csDBLock)
	, m_hSaveEvent(hEvent)
	, m_pLogFile(NULL)
{
	m_bAlive = FALSE;
	m_pDisplayBuffer = new CFMDisplayDataBuffer();
	Reset();

	if (theApp.DebugSocket() ||
		theApp.DebugStorage()) {
		m_tmRecordStart = CTime::GetCurrentTime();
		CString sLogFile;
		sLogFile.Format(_T("%s/SYSTEM/log%02d.txt"), theApp.GetDataRoot(), nIndex + 1);
		m_pLogFile = new CLogFile(sLogFile);
	}

	m_sMedicalNum = _T("");
}

void CFMRecordUnit::WriteLogString(CString sFunction, CString sContent)
{
	if (! theApp.DebugStorage()) {
		return;
	}

	if (! m_pLogFile) {
		return;
	}
	CString sLogInfo = _T("");

	//时间
	CTime tmNow = CTime::GetCurrentTime();
	CTimeSpan ts = tmNow - m_tmRecordStart;
	CString sTimeInfo = ts.Format(_T("%H:%M:%S"));
	while (sTimeInfo.GetLength() < 10) {
		sTimeInfo += _T(" ");
	}
	sLogInfo += sTimeInfo;

	//函数名
	while (sFunction.GetLength() < 40) {
		sFunction += _T(" ");
	}
	sLogInfo += sFunction;

	//其他信息
	sLogInfo += sContent;

	sLogInfo.Format(_T("%s%s%s"), sTimeInfo, sFunction, sContent);
	if (sLogInfo.Right(1) != _T("\n")) {
		sLogInfo += _T("\n");
	}

	m_pLogFile->Write(sLogInfo);
}

void CFMRecordUnit::Reset()
{
	m_sRecordID = _T("");
	m_sMedicalNum = _T("");
	ZeroMemory(m_pcdSaveBuffer, sizeof(m_pcdSaveBuffer));
	m_nSavePos = 0;
	m_nSaveCount = 0;
	m_nStartOffset = 0;
	m_bWaitForSave = FALSE;
	m_nFileCount = 0;
	m_type = MONITOR_TYPE_FETUS;
	m_bFirst = TRUE;
	m_pDisplayBuffer->ResetBuffer();
}

//释放过时的数据
void CFMRecordUnit::ReleaseOutdatedBuffer()
{
	if (m_bWaitForSave) {
		return;
	}
	if (m_nSavePos > RECORD_RELEASE_SIZE) {
		WriteLogString(_T("CFMRecordUnit::ReleaseOutdatedBuffer"), _T("释放过期数据缓冲。"));
		CSmartLock lock(m_csRecvBufLock);

		int nReleaseSize = m_nSavePos;
		m_nSavePos = 0;
		m_nStartOffset += nReleaseSize;
		m_arRecvBuf.RemoveAt(0, nReleaseSize);
	}
}

CFMRecordUnit::~CFMRecordUnit(void)
{
	//停止，保存尚未保存的数据。
	if (m_bAlive) {
		Stop();
	}

	if (! m_arRecvBuf.IsEmpty()) {
		m_arRecvBuf.RemoveAll();
	}
	if (m_pDisplayBuffer) {
		delete m_pDisplayBuffer;
		m_pDisplayBuffer = NULL;
	}

	if (m_pLogFile) {
		delete m_pLogFile;
		m_pLogFile = NULL;
	}
}

BOOL CFMRecordUnit::SocketAdd(CLIENT_DATA& cd)
{
	if (!m_bAlive) {
		return FALSE;
	}

	{
		CSmartLock lock(m_csRecvBufLock);
		m_arRecvBuf.Add(cd);
		m_pDisplayBuffer->Add(cd);

		int nSize = (int)m_arRecvBuf.GetSize();
		if ((nSize - m_nSavePos) > RECORD_SAVE_SIZE) {
			//触发存盘动作
			TriggerSaveProcess();
		}
	}
	
	return TRUE;
}

int CFMRecordUnit::GetDisplayLenOfSecond()
{
	if (! m_pDisplayBuffer) {
		return 0;
	}
	return m_pDisplayBuffer->GetTimeSpan();
}

//获取指定时间段内的显示数据
DISPLAY_DATA* CFMRecordUnit::GetNumbData(int nTimeSpan)
{
	if (! m_pDisplayBuffer) {
		return NULL;
	}
	return m_pDisplayBuffer->GetNumbData(nTimeSpan);
}
DISPLAY_DATA* CFMRecordUnit::GetLatestNumbData()
{
	if (! m_pDisplayBuffer) {
		return NULL;
	}
	return m_pDisplayBuffer->GetLatestNumbData();
}
void CFMRecordUnit::Autodiagnostics(void)
{
	if (! m_pDisplayBuffer) {
		return;
	}
	return m_pDisplayBuffer->Autodiagnostics();
}
int CFMRecordUnit::GetWaveData(int nTimeSpan,
	BYTE** ppECG1, BYTE** ppECG2, BYTE** ppECGv, BYTE** ppSPO2, BYTE** ppRESP)
{
	if (! m_pDisplayBuffer) {
		return FALSE;
	}
	return m_pDisplayBuffer->GetWaveData(nTimeSpan,
		ppECG1, ppECG2, ppECGv,	ppSPO2, ppRESP);
}
int CFMRecordUnit::GetFHRWaveData(int nTimeSpan,
	WORD** ppFHR1, WORD** ppFHR2, WORD** ppTOCO, BYTE** ppFMMARK, BYTE** ppTOCOReset, DISPLAY_FLAG** ppDiagno1, DISPLAY_FLAG** ppDiagno2)
{
	if (! m_pDisplayBuffer) {
		return FALSE;
	}
	return m_pDisplayBuffer->GetFHRWaveData(nTimeSpan,
		ppFHR1, ppFHR2, ppTOCO, ppFMMARK, ppTOCOReset, ppDiagno1, ppDiagno2);
}

//触发保存过程，并进行必要的处理
BOOL CFMRecordUnit::TriggerSaveProcess()
{
	if (m_sRecordID.IsEmpty()) {
		//没有记录号，不能保存
		WriteLogString(_T("CFMRecordUnit::TriggerSaveProcess"), _T("没有有效记录号，不能保存！"));
		return FALSE;
	}
	if (m_bWaitForSave) {
		WriteLogString(_T("CFMRecordUnit::TriggerSaveProcess"), _T("m_bWaitForSave！"));
		return FALSE;
	}

	{
		//将数据拷贝至存盘buffer
		WriteLogString(_T("CFMRecordUnit::TriggerSaveProcess"), _T("将数据拷贝到存盘缓冲。"));
		CSmartLock lock(m_csRecvBufLock);

		int i;
		int nSize = (int)m_arRecvBuf.GetSize();
		m_nSaveCount = min(RECORD_SAVE_SIZE, nSize - m_nSavePos);
		for (i=0; i<m_nSaveCount; i++) {
			CLIENT_DATA& cd = m_arRecvBuf[m_nSavePos];
			memcpy(&(m_pcdSaveBuffer[i]), &cd, sizeof(CLIENT_DATA));
			m_nSavePos ++;
		}
	}

	//设置事件，然后交给FMRecordThread的工作线程去处理
	if (m_nSaveCount > 0) {
		m_bWaitForSave = TRUE;
		SetEvent(m_hSaveEvent);
		WriteLogString(_T("CFMRecordUnit::TriggerSaveProcess"), _T("设置触发事件m_hSaveEvent。"));
	}
	else {
		WriteLogString(_T("CFMRecordUnit::TriggerSaveProcess"), _T("没有有效数据，未设置触发事件m_hSaveEvent。"));
	}

	return TRUE;
}

//执行将数据保存为文件的操作
void CFMRecordUnit::SaveUnitProcess()
{
	WriteLogString(_T("CFMRecordUnit::SaveUnitProcess"), _T("开始存盘操作。"));
	int nOffset = m_nStartOffset + m_nSavePos - m_nSaveCount;//相对于起始位置的偏移量

	//生成文件名，写文件，并写入数据库
	CTime tm = m_tmStart;// + CTimeSpan(0, 0, 0, nOffset); //1个记录就等于1秒钟

	CString sDate = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	//文件名的格式是：(床号:文件号)年-月-日:时:分:秒.rcd
	CString sFileName;
	sFileName.Format(_T("%04d%02d%02d-%02d%02d%02d(%02d-%d).rcd"),
		tm.GetYear(), tm.GetMonth(), tm.GetDay(), tm.GetHour(), tm.GetMinute(), tm.GetSecond(),
		m_nIndex+1, m_nFileCount+1);
	CString sFullName;
	sFullName.Format(_T("%s%s/%s"), theApp.GetDataRoot(), RECORD_DIR, sFileName);

	//写入文件
	CFile file(sFullName, CFile::modeReadWrite | CFile::modeCreate | CFile::typeBinary);
	file.Write(m_pcdSaveBuffer, sizeof(CLIENT_DATA)*m_nSaveCount);
	file.Flush();
	file.Close();
	WriteLogString(_T("CFMRecordUnit::SaveUnitProcess"), _T("存盘完成。"));

	//添加数据库记录
	CString sql;
	sql.Format(
		_T("INSERT file_record (foffset,fsize,fname,mrid) ")
		_T("VALUES ('%d','%d','%s','%s');"),
		nOffset,      //相对于起始位置的偏移量
		m_nSaveCount, //保存了多少个记录
		sFileName,    //文件名
		m_sRecordID); //对应的记录号

	OutputDebugString(sql + _T("\n"));
	WriteLogString(_T("CFMRecordUnit::SaveUnitProcess"), sql);

	{
		// 执行SQL语句
		CSmartLock lock(m_dbLock);
		try {
			m_db.ExecuteSQL(sql);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMRecordUnit::SaveUnitProcess第1个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
		}
	}
	WriteLogString(_T("CFMRecordUnit::SaveUnitProcess"), _T("存数据库完成。"));

	// 设置标志
	m_nSaveCount = 0;
	m_nFileCount ++;
	m_bWaitForSave = FALSE;

	// 存盘工作完成后，检查是否有可以释放的数据
	ReleaseOutdatedBuffer();
}

BOOL CFMRecordUnit::Start()
{
	if (theApp.DebugUI()) {
		if (m_bAlive) {
			return FALSE;
		}
		Reset();
		m_bAlive = TRUE;
		theApp.AddAliveIndex(m_nIndex);
		WriteLogString(_T("CFMRecordUnit::Start"), _T("在TEST_MODE_UI模式下，不创建数据库记录。"));
		return TRUE;
	}

	//还没有完成存盘工作，不能开始新的记录
	if (m_bWaitForSave) {
		WriteLogString(_T("CFMRecordUnit::Start"), _T("还没有完成存盘工作，不能开始新的记录。"));
		return FALSE;
	}

	Reset();
	m_bAlive = TRUE;
	m_tmStart = CTime::GetCurrentTime();

	//得到新的rid
	GetNewRecordID();
	if (m_sRecordID.IsEmpty()) {
		return FALSE;
	}
	
	//在数据库中创建一个新的记录
	CString sDate = m_tmStart.Format(_T("%Y-%m-%d %H:%M:%S"));
	CString sql;
	sql.Format(
		_T("INSERT monitor_record (mrid,bed_num,create_date,operator,med_num) ")
		_T("VALUES ('%s','%02d','%s','%s',NULL);"),
		m_sRecordID,
		m_nIndex+1,  //床号
		sDate,       //创建时间
		theApp.m_sAdminName);

	OutputDebugString(sql + _T("\n"));

	{
		// 执行SQL语句
		CSmartLock lock(m_dbLock);
		try {
			m_db.ExecuteSQL(sql);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMRecordUnit::Start第1个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return FALSE;
		}
	}

	WriteLogString(_T("CFMRecordUnit::Start"), sql);

	if (theApp.DebugStorage()) {
		m_tmRecordStart = CTime::GetCurrentTime();
	}
	return TRUE;
}

BOOL CFMRecordUnit::SetMedicalNum(CWnd* pWnd, CString& sMedNum)
{
	CString sql;
	sql.Format(
		_T("UPDATE monitor_record SET med_num='%s',operator='%s' ")
		_T("WHERE mrid='%s'"),
		sMedNum, theApp.m_sAdminName, m_sRecordID);

	//执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		theApp.m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMRecordUnit::SetMedicalNum第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		pWnd->MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return FALSE;
	}
	m_sMedicalNum = sMedNum;
	UpdatePatientInfo(sMedNum);
	return TRUE;
}

void CFMRecordUnit::UpdatePatientInfo(CString sMedicalNum)
{
	CRecordset rs(&m_db);
	CString sql;
	sql.Format(
		_T("SELECT patient_name,patient_type,gender,birthday ")
		_T("FROM medical_record WHERE medical_num = '%s'"), sMedicalNum);

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMRecordUnit::UpdatePatientInfo第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMRecordUnit::UpdatePatientInfo第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
	if (!rs.IsEOF()) {
		try {
			CTime tm;
			CString sValue;
			CDBVariant var;

			rs.GetFieldValue(_T("patient_name"), sValue);
			m_sPatientName = sValue;

			rs.GetFieldValue(_T("patient_type"), var);
			m_nPatientType = var.m_lVal;

			rs.GetFieldValue(_T("gender"), var);
			m_nPatientGender = var.m_lVal;

			rs.GetFieldValue(_T("birthday"), var);
			tm = CTime((var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
				(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);
			m_nPatientAge = GetAge(tm);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMRecordUnit::UpdatePatientInfo第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}
	}

	if (rs.IsOpen()) {
		rs.Close();
	}
}

void CFMRecordUnit::Stop()
{
	WriteLogString(_T("CFMRecordUnit::Stop"), _T("开始存盘工作。"));
	theApp.DelAliveIndex(m_nIndex);

	if (! m_bAlive) {
		return;
	}

	//设置标志宣告停止活动
	m_bAlive = FALSE;

	//保存缓冲中的数据
	int nRetryTimes = 20;
	while (nRetryTimes > 0) {
		TriggerSaveProcess();
		if (m_nSaveCount > 0) {
			//等待存盘结束
			while(m_bWaitForSave) {
				Sleep(100);
			}
			nRetryTimes --;
		}
		else {
			break;
		}
	}
	WriteLogString(_T("CFMRecordUnit::Stop"), _T("存盘结束。"));
}

void CFMRecordUnit::GetNewRecordID()
{
	CRecordset rs(&m_db);
	CString sql = _T("SELECT TOP 1 mrid FROM monitor_record ORDER BY mrid DESC");

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMRecordUnit::GetNewRecordID第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		m_sRecordID = DEFAULT_RECORD_ID_START;
		return;
	}

	// 读出内容为最大的rid
	if (!rs.IsEOF()) {
		rs.MoveFirst();
		try {
			rs.GetFieldValue(_T("mrid"), m_sRecordID);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMRecordUnit::GetNewRecordID第2个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			m_sRecordID = DEFAULT_RECORD_ID_START;
			return;
		}
	}
	else {
		m_sRecordID = DEFAULT_RECORD_ID_START;
		return;
	}

	if (rs.IsOpen()) {
		rs.Close();
	}

	//数据库中查出的最大ID号加一
	IncreaseStringNum(m_sRecordID);
}

