// SetupTEMPDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SetupTEMPDlg.h"
#include "afxdialogex.h"
#include "FMSocketThread.h"
#include "FMSocketUnit.h"
#include "FMRecordUnit.h"


// CSetupTEMPDlg 对话框

IMPLEMENT_DYNAMIC(CSetupTEMPDlg, CDialogEx)

CSetupTEMPDlg::CSetupTEMPDlg(CWnd* pParent, CFMRecordUnit* pru, int nTEMPUnit)
	: CDialogEx(CSetupTEMPDlg::IDD, pParent)
	, m_pru(pru)
{
	m_nTEMPUnitOpt = nTEMPUnit;
}

CSetupTEMPDlg::~CSetupTEMPDlg()
{
}

void CSetupTEMPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_T1_HIGH, m_edtT1High);
	DDX_Control(pDX, IDC_EDIT_T1_LOW, m_edtT1Low);
	DDX_Control(pDX, IDC_EDIT_T2_HIGH, m_edtT2High);
	DDX_Control(pDX, IDC_EDIT_T2_LOW, m_edtT2Low);
	DDX_Control(pDX, IDC_COMBO_TEMP_UNIT, m_cmbTEMPUnit);
	DDX_Control(pDX, IDC_STA_RANGE1, m_staRange1);
	DDX_Control(pDX, IDC_STA_RANGE2, m_staRange2);
	DDX_Control(pDX, IDC_STA_RANGE3, m_staRange3);
	DDX_Control(pDX, IDC_STA_RANGE4, m_staRange4);
}

BEGIN_MESSAGE_MAP(CSetupTEMPDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSetupTEMPDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSetupTEMPDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_SEND, &CSetupTEMPDlg::OnBnClickedSend)
END_MESSAGE_MAP()

// CSetupTEMPDlg 消息处理程序
void CSetupTEMPDlg::OnBnClickedSend()
{
	// 发送设置
	CString sValue;
	m_edtT1High.GetWindowText(sValue); //温度，统一转换成摄氏度进行处理
	int t1_h = (int)(1 == m_nTEMPUnitOpt ? (_ttof(sValue) * 10 - 320) * 5 / 9 : _ttof(sValue) * 10);
	m_edtT1Low.GetWindowText(sValue);
	int t1_l = (int)(1 == m_nTEMPUnitOpt ? (_ttof(sValue) * 10 - 320) * 5 / 9 : _ttof(sValue) * 10);
	m_edtT2High.GetWindowText(sValue);
	int t2_h = (int)(1 == m_nTEMPUnitOpt ? (_ttof(sValue) * 10 - 320) * 5 / 9 : _ttof(sValue) * 10);
	m_edtT2Low.GetWindowText(sValue);
	int t2_l = (int)(1 == m_nTEMPUnitOpt ? (_ttof(sValue) * 10 - 320) * 5 / 9 : _ttof(sValue) * 10);

	if (t1_h < 0 || t1_h > 500) {
		MessageBox(_T("体温1上限超出范围！"));
		m_edtT1High.SetFocus();
		return;
	}
	if (t1_l < 0 || t1_l > 500) {
		MessageBox(_T("体温1下限超出范围！"));
		m_edtT1Low.SetFocus();
		return;
	}
	if (t1_l >= t1_h) {
		MessageBox(_T("体温1下限不能超越上限设定！"));
		m_edtT1Low.SetFocus();
		return;
	}
	if (t2_h < 0 || t2_h > 500) {
		MessageBox(_T("体温2上限超出范围！"));
		m_edtT2High.SetFocus();
		return;
	}
	if (t2_l < 0 || t2_l > 500) {
		MessageBox(_T("体温2下限超出范围！"));
		m_edtT2Low.SetFocus();
		return;
	}
	if (t2_l >= t2_h) {
		MessageBox(_T("体温2下限不能超越上限设定！"));
		m_edtT2Low.SetFocus();
		return;
	}

	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	psu->SendSetupTEMP(t1_h, t1_l, t2_h, t2_l);
}

void CSetupTEMPDlg::OnBnClickedOk()
{
	m_nTEMPUnitOpt = m_cmbTEMPUnit.GetCurSel();
	CDialogEx::OnOK();
}

void CSetupTEMPDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}

BOOL CSetupTEMPDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_cmbTEMPUnit.ResetContent();
	int i;
	int count = CFMDict::GetTEMPUnitCount();
	for (i=0; i<count; i++) {
		m_cmbTEMPUnit.AddString(CFMDict::TEMPUnit_itos(i));
	}
	m_cmbTEMPUnit.SetCurSel(m_nTEMPUnitOpt);

	DISPLAY_DATA* pdd = m_pru->GetLatestNumbData();
	if (pdd) {
		CString sValue;
		if (1 == m_nTEMPUnitOpt) {
			//转换为华氏度
			sValue.Format(_T("%.1f"), ((float)(pdd->t1_h * 9)) / 50.0f + 32.0f);
			m_edtT1High.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), ((float)(pdd->t1_h * 9)) / 50.0f + 32.0f);
			m_edtT1Low.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), ((float)(pdd->t1_h * 9)) / 50.0f + 32.0f);
			m_edtT2High.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), ((float)(pdd->t1_h * 9)) / 50.0f + 32.0f);
			m_edtT2Low.SetWindowText(sValue);
		}
		else {
			sValue.Format(_T("%.1f"), ((float)(pdd->t1_h)) / 10.0f);
			m_edtT1High.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), ((float)(pdd->t1_l)) / 10.0f);
			m_edtT1Low.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), ((float)(pdd->t2_h)) / 10.0f);
			m_edtT2High.SetWindowText(sValue);
			sValue.Format(_T("%.1f"), ((float)(pdd->t2_l)) / 10.0f);
			m_edtT2Low.SetWindowText(sValue);
		}
	}

	if (1 == m_nTEMPUnitOpt) {
		//转换为华氏度
		m_staRange1.SetWindowText(_T("（96.8~102.3℉）"));
		m_staRange2.SetWindowText(_T("（96.8~102.3℉）"));
		m_staRange3.SetWindowText(_T("（96.8~102.3℉）"));
		m_staRange4.SetWindowText(_T("（96.8~102.3℉）"));
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


