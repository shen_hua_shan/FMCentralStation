#pragma once
#include "reviewdlg.h"
class CReviewTendWave : public CReviewTool
{
private:
	CFont m_fontInfo;
	CFont m_fontTableScale;
	CFont* m_pOldFont;

public:
	CReviewTendWave(CReviewDlg* pdlg);
	virtual ~CReviewTendWave(void);

private:
	virtual int  GetPageSpan();
	virtual void EnterReview();
	virtual void LeaveReview();
	virtual void Draw(CDC* pdc);

	void DrawGrid(CDC* pdc, CRect& rc);
	void DrawInfo(CDC* pdc, CRect& rc);
	void DrawTime(CDC* pdc, CRect& rc);
	void DrawTimeRuler(CDC* pdc, int x, int y, CTime tm, int nSeconds);
	void DrawData(CDC* pdc, CRect& rc);
};

