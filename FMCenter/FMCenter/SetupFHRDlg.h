#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CSetupFHRDlg 对话框
class CFMRecordUnit;
class CSetupFHRDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetupFHRDlg)

private:
	CFMRecordUnit* m_pru;
	int m_nFHRPaterSpeedOpt;

private:
	void MonitorRecordSetPregnantWeek(CString& sMRID, CString& sPregnantWeek);

public:
	CSetupFHRDlg(CWnd* pParent, CFMRecordUnit* pru, int nPaperSpeed);
	virtual ~CSetupFHRDlg();

	inline int GetFHRPaperSpeedOpt() { return m_nFHRPaterSpeedOpt; };

// 对话框数据
	enum { IDD = IDD_DIALOG_SETUP_FHR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edtFHRHigh;
	CEdit m_edtFHRLow;
	CComboBox m_cmbPaperSpeed;
	CEdit m_edtPregnantWeek;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedSend();
	afx_msg void OnBnClickedBtnSavePregnantWeek();
};
