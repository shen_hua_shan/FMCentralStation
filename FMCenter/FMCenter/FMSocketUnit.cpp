#include "stdafx.h"
#include "LogFile.h"
#include "FMCenter.h"
#include "FMRecordUnit.h"
#include "FMSocketUnit.h"


CFMSocketUnit::CFMSocketUnit(int nIndex)
	: m_pRecordUnit(theApp.GetRecordUnit(nIndex))
{
	m_bValid = FALSE;
	m_op = OP_RECV;
	m_nSendTotal = 0;
	m_nRecvLen = 0;
	m_nSendLen = 0;

	m_nIndex = nIndex;

	ZeroMemory(m_psUserBuf, SOCKET_BUFFER_SIZE);
	m_nUserBufLen = 0;

	ResetSendOverlap();
	ResetRecvOverlap();
	ResetSendWSABUF();
	ResetRecvWSABUF();

	if (theApp.DebugSocket()) {
		CTime m_tmCreate   = CTime::GetCurrentTime();
		CTime m_tmLastRecv = CTime::GetCurrentTime();
	}
	m_tmRecv = CTime::GetCurrentTime();
}

CFMSocketUnit::~CFMSocketUnit(void)
{
	if (SOCKET_ERROR != m_so) {
		closesocket(m_so);
		m_so = SOCKET_ERROR;
	}
}

void CFMSocketUnit::WriteLogString(CString sFunction, CString sContent)
{
	if (! theApp.DebugSocket()) {
		return;
	}

	CString sLogInfo = _T("");

	//时间
	CTime tmNow = CTime::GetCurrentTime();
	CTimeSpan ts = tmNow - m_tmCreate;
	CString sTimeInfo = ts.Format(_T("%H:%M:%S"));
	while (sTimeInfo.GetLength() < 10) {
		sTimeInfo += _T(" ");
	}
	sLogInfo += sTimeInfo;

	//函数名
	while (sFunction.GetLength() < 40) {
		sFunction += _T(" ");
	}
	sLogInfo += sFunction;

	//其他信息
	sLogInfo += sContent;

	sLogInfo.Format(_T("%s%s%s"), sTimeInfo, sFunction, sContent);
	if (sLogInfo.Right(1) != _T("\n")) {
		sLogInfo += _T("\n");
	}

	if (! m_pRecordUnit) {
		return;
	}
	m_pRecordUnit->m_pLogFile->Write(sLogInfo);
}

void CFMSocketUnit::Enable(BOOL bEn)
{
	m_bValid = bEn;
	if (bEn) {
		theApp.AddAliveIndex(m_nIndex);
		theApp.PostLayoutMessage();
	}
	else {
		//为什么要在这里关闭socket呢？
		//因为windows下socket即使关闭了，完成端口还是有可能收到数据。
		//那边收到数据就会刷新显示，会使界面灰了又亮起，闪一下。
		//在这里关闭socket就稳妥了。
		CloseSocket();
		theApp.DelAliveIndex(m_nIndex);
		theApp.PostLayoutMessage();
	}
}

BOOL CFMSocketUnit::CheckSocket(CTime tmNow)
{
	if (m_bValid) {
		CTimeSpan ts = tmNow - m_tmRecv;
		if (ts.GetSeconds() > 10) {
			return FALSE;
		}
	}
	return TRUE;
}

int CFMSocketUnit::OnReceive()
{
	m_tmRecv = CTime::GetCurrentTime();
	return ReceiveProc(m_szRecvBuf, m_nRecvLen);
}

void CFMSocketUnit::ResetSendWSABUF()
{
	ZeroMemory(m_szSendBuf, sizeof(m_szSendBuf));
	m_wbuf_send.len = 0;
	m_wbuf_send.buf = m_szSendBuf;
}

void CFMSocketUnit::ResetRecvWSABUF()
{
	ZeroMemory(m_szRecvBuf, sizeof(m_szRecvBuf));
	m_wbuf_recv.len = sizeof(m_szRecvBuf);
	m_wbuf_recv.buf = m_szRecvBuf;
}

void CFMSocketUnit::ResetSendOverlap()
{
	ZeroMemory((void *)&m_ol_send, sizeof(m_ol_send));
}

void CFMSocketUnit::ResetRecvOverlap()
{
	ZeroMemory((void *)&m_ol_recv, sizeof(m_ol_recv));
}

void CFMSocketUnit::OnSend()
{
}

void CFMSocketUnit::SetSocket(SOCKET s)
{
	m_so = s;

	m_op = OP_RECV;
	m_nSendTotal = 0;
	m_nRecvLen = 0;
	m_nSendLen = 0;

	ZeroMemory(m_psUserBuf, SOCKET_BUFFER_SIZE);
	m_nUserBufLen = 0;

	ResetSendOverlap();
	ResetRecvOverlap();
	ResetSendWSABUF();
	ResetRecvWSABUF();
}

int CFMSocketUnit::ReceiveProc(char* buf, int size)
{
	int i, j;
	int parse_ret;
	int data_size;
	int expect_size;

	//接着上次的位置收数据
	memcpy(&(m_psUserBuf[m_nUserBufLen]), buf, size);
	m_nUserBufLen += size;

	CString info;
	info.Format(_T("\nReceiveProc，床号：%d\n"), m_nIndex+1);
	OutputDebugString(info);

	while (TRUE) {
		parse_ret = CStructTools::CheckData(m_psUserBuf, m_nUserBufLen, &data_size, &expect_size);
		if (parse_ret > 0) {
			CString info;
			info.Format(_T("CheckData 残留数据：%d字节\n"), parse_ret);
			OutputDebugString(info);
			//在“包起始标志”之前有parse_ret个字节的残留数据
			m_nUserBufLen -= parse_ret;
			for (i = 0, j = parse_ret; i < m_nUserBufLen; i++, j++) {
				m_psUserBuf[i] = m_psUserBuf[j];
			}
			//通知：清除了一定数量的无效数据。（以负数约定此类错误）
			OnReceiveError(-parse_ret);
			//去除多余数据后，应再次分析数据是否符合要求
			parse_ret = CStructTools::CheckData(m_psUserBuf, m_nUserBufLen, &data_size, &expect_size);
		}
		if (parse_ret == 0) {
			CString info;
			info.Format(_T("CheckData OK，可以处理数据。\n"));
			OutputDebugString(info);
			//正常，可以处理数据
			OnUserDataReceive((SOCK_PKG*)m_psUserBuf);

			//删除已经处理过的数据
			m_nUserBufLen -= data_size;
			for (i = 0, j = data_size; i < m_nUserBufLen; i++, j++) {
				m_psUserBuf[i] = m_psUserBuf[j];
			}
			if (m_nUserBufLen > 0) {
				continue;  //m_psBuf中还有数据没处理完
			}
			else {
				break;  //完美结束
			}
		}
		if (parse_ret == -1) {
			CString info;
			info.Format(_T("CheckData 期待数据：%d字节\n"), expect_size);
			OutputDebugString(info);
			//表示还没接收完，继续
			break;
		}
		if (parse_ret == -2) {
			CString info;
			info.Format(_T("！！！CheckData 出错，后面没法处理。\n"));
			OutputDebugString(info);
			theApp.WriteLog(_T("CFMSocketUnit::ReceiveProc()函数\n"));
			theApp.WriteLog(info);
			//接收出错，后面的没法处理了，清缓冲区。（以负数约定此类错误）
			OnReceiveError(-m_nUserBufLen);
			m_nUserBufLen = 0;
		}
	}

	//继续接收，或者接收下一个数据包头
	if (expect_size > 0) {
		CString info;
		info.Format(_T("期待字节：%d\n"), expect_size);
		OutputDebugString(info);
		return expect_size;
	}
	else {
		CString info;
		info.Format(_T("期待字节：%d\n"), sizeof(HEAD));
		OutputDebugString(info);
		return sizeof(HEAD);
	}
}

BOOL CFMSocketUnit::SendData(BYTE* buf, int len)
{
	DWORD dwBytes = 0;
	int   nRet = 0;

	m_op |= OP_SEND;
	ResetSendWSABUF();
	ResetSendOverlap();
	m_nSendLen = 0;

	memcpy(m_szSendBuf, buf, len);
	m_nSendTotal = len;
	m_wbuf_send.len = min(m_nSendTotal, SOCKET_BUFFER_SIZE);

	//Overlapped send
	nRet = WSASend(m_so, &m_wbuf_send, 1, 
		(LPDWORD)&dwBytes, 0, &m_ol_send, NULL);

	if ((SOCKET_ERROR == nRet) && (WSA_IO_PENDING != WSAGetLastError())) {
		return FALSE;
	}

	return TRUE;
}

void CFMSocketUnit::OnReceiveError(int nErrorNo)
{
	CString err;
	err.Format(_T("nErrorNo: %d"), nErrorNo);
	WriteLogString(_T("CFMSocketUnit::OnReceiveError"), err);
}

void CFMSocketUnit::OnUserDataReceive(SOCK_PKG* pk)
{
	int bed_num = pk->head.bed_num;
	CString info;
	info.Format(_T("\n单元%d收到数据，床号: %d\n"), GetBedNum(), bed_num);
	OutputDebugString(info);
	if (bed_num != GetBedNum()) {
		//不记录了，这个错误不重要
		//theApp.WriteLog(_T("CFMSocketUnit::OnUserDataReceive, 床号不对！"));
		//theApp.WriteLog(info);
		//theApp.WriteLog(_T("\n"));
	}

	switch (pk->head.pkg_type) {

	case PKG_CLIENT_DATA:
		info.Format(_T("PKG_CLIENT_DATA，长度: %d\n"), pk->head.body_size);
		OutputDebugString(info);
		if (theApp.DebugSocketDetail()) {
			CString sDump = CStructTools::DumpClientData(pk);
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("-----------------------------------------------------\n"));
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), sDump);
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n"));
		}

		if (theApp.DebugSocket()) {
			CTime tmNow = CTime::GetCurrentTime();
			CTimeSpan ts = tmNow - m_tmLastRecv;
			int span = (int)ts.GetTotalSeconds();
			info.Format(_T("PKG_CLIENT_DATA，time_span: %d，length: %d\n"),
				span, pk->head.body_size);
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), info);
			m_tmLastRecv = tmNow;
		}

		//处理数据包
		if (CStructTools::IsNullData(&(pk->body.cd))) {
			//空包，表示结束监护
			if (CStructTools::SendQuit(this)) {
				info = _T("发送 QUIT 成功。\n");
				WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), info);
			}
			else {
				info = _T("！！！发送 QUIT 失败。\n");
				theApp.WriteLog(_T("CFMSocketUnit::OnUserDataReceive, 收到NullData处理："));
				theApp.WriteLog(info);
				theApp.WriteLog(_T("\n"));
			}
			//完成善后工作
			m_pRecordUnit->Stop();
		}
		else {
			//保存数据
			m_pRecordUnit->SocketAdd(pk->body.cd);
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("保存数据 完成。"));

			if (CStructTools::SendNext(this)) {
				info = _T("发送 NEXT 成功。\n");
				WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), info);
			}
			else {
				info = _T("！！！发送 NEXT 失败。\n");
				theApp.WriteLog(_T("CFMSocketUnit::OnUserDataReceive, PKG_CLIENT_DATA处理："));
				theApp.WriteLog(info);
				theApp.WriteLog(_T("\n"));
			}
		}
		OutputDebugString(info);
		break;

	case PKG_PATIENTINFO:
		WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("收到 PKG_PATIENTINFO包。"));
		if (theApp.DebugSocketDetail()) {
			CString sDump = CStructTools::DumpPatientInfo(pk);
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("-----------------------------------------------------\n"));
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), sDump);
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n"));
		}

		if (theApp.DebugSocket()) {
			m_tmLastRecv = CTime::GetCurrentTime();
		}

		//开始记录
		{
			//防止在判断IsAlive和Start中间插入操作，造成一床开两个窗口现象
			CSmartLock lock(m_csOpLock);
			if (m_pRecordUnit->IsAlive()) {
				WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("正常监护过程中，又收到 握手 包，不处理。"));
			}
			else {
				m_pRecordUnit->Start();
				WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), _T("启动 RecordUnit的 Start函数。"));
			}
		}

		//告诉客户端，收到，继续吧。
		if (CStructTools::SendNext(this)) {
			info = _T("发送 NEXT 成功。\n");
			WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), info);
		}
		else {
			info = _T("！！！发送 NEXT 失败。\n");
			theApp.WriteLog(_T("CFMSocketUnit::OnUserDataReceive, PKG_PATIENTINFO处理："));
			theApp.WriteLog(info);
			theApp.WriteLog(_T("\n"));
		}
		OutputDebugString(info);
		break;

	case PKG_QUIT:
		//结束监护
		m_pRecordUnit->Stop();

		CStructTools::DumpHead(pk);
		info = _T("收到 QUIT 数据包\n");
		OutputDebugString(info);
		WriteLogString(_T("CFMSocketUnit::OnUserDataReceive"), info);
		break;

	default:
		//程序不会走到这里
		break;
	}
}

//关闭Socket
void CFMSocketUnit::CloseSocket(void)
{
	LINGER lingerStruct;
	lingerStruct.l_onoff = 1;
	lingerStruct.l_linger = 0;
	setsockopt(m_so, SOL_SOCKET, SO_LINGER, (char *)&lingerStruct, sizeof(lingerStruct));
	closesocket(m_so);
	m_so = SOCKET_ERROR;
}

void CFMSocketUnit::OnConnect()
{
	WriteLogString(_T("CFMSocketUnit::OnConnect"), _T(""));
}

BOOL CFMSocketUnit::SendSetupECG(
	int ecg_l1, int ecg_l2, int filter, int gain, int stop, int paceangy, int stangy, int rhythm, 
	int hr_h, int hr_l, int st1_h, int st1_l, int st2_h, int st2_l, int pvc_h, int pvc_l)
{
	return CStructTools::SendSetupECG(this,
		ecg_l1, ecg_l2, filter, gain, stop, paceangy, stangy, rhythm, 
		hr_h, hr_l, st1_h, st1_l, st2_h, st2_l, pvc_h, pvc_l);
}

BOOL CFMSocketUnit::SendSetupNIBP(int sys_h, int sys_l, int mea_h, int mea_l, int dia_h, int dia_l, int nibp_mode)
{
	return CStructTools::SendSetupNIBP(this, sys_h, sys_l, mea_h, mea_l, dia_h, dia_l, nibp_mode);
}

BOOL CFMSocketUnit::SendSetupRESP(int rr_h, int rr_l)
{
	return CStructTools::SendSetupRESP(this, rr_h, rr_l);
}

BOOL CFMSocketUnit::SendSetupSPO2(int spo2_h, int spo2_l, int pr_h, int pr_l)
{
	return CStructTools::SendSetupSPO2(this, spo2_h, spo2_l, pr_h, pr_l);
}

BOOL CFMSocketUnit::SendSetupTEMP(int t1_h, int t1_l, int t2_h, int t2_l)
{
	return CStructTools::SendSetupTEMP(this, t1_h, t1_l, t2_h, t2_l);
}

BOOL CFMSocketUnit::SendNIBPPumpCmd(int cmd)
{
	return CStructTools::SendNIBPPumpCmd(this, cmd);
}

BOOL CFMSocketUnit::SendSetupFHR(int fhr_h, int fhr_l)
{
	return CStructTools::SendSetupFHR(this, fhr_h, fhr_l);
}
