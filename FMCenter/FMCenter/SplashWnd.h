#pragma once
#include "afxwin.h"
class CSplashWnd : public CWnd
{
public:
	CSplashWnd(void);
	virtual ~CSplashWnd(void);
	virtual void PostNcDestroy();

public:
    CBitmap m_bitmap;

protected:
    static BOOL c_bShowSplashWnd;
    static CSplashWnd* c_pSplashWnd;

public:
    static void EnableSplashScreen(BOOL bEnable = TRUE);
    static void ShowSplashScreen(CWnd* pParentWnd = NULL);
    static BOOL PreTranslateAppMessage(MSG* pMsg);

protected:
    BOOL Create(CWnd* pParentWnd = NULL);
    void HideSplashScreen();

    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnPaint();
    afx_msg void OnTimer(UINT nIDEvent);

protected:
    DECLARE_MESSAGE_MAP()
};

