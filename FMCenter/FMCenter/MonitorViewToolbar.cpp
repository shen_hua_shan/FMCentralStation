// MonitorViewToolbar.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "MonitorViewToolbar.h"
#include "afxdialogex.h"
#include "FMMonitorView.h"


// CMonitorViewToolbar 对话框

IMPLEMENT_DYNAMIC(CMonitorViewToolbar, CDialogEx)

CMonitorViewToolbar::CMonitorViewToolbar(CFMMonitorView* pParent)
	: CDialogEx(CMonitorViewToolbar::IDD, pParent)
{
	m_pMonitorView = pParent;
	Create(IDD, pParent);
}

CMonitorViewToolbar::~CMonitorViewToolbar()
{
}

void CMonitorViewToolbar::SetStatus(int x, int y, CString type)
{
	CRect rc;
	GetWindowRect(&rc);
	ShowWindow(SW_HIDE);
	MoveWindow(x, y, rc.Width(), rc.Height());
	m_btnSwitchType.SetWindowText(type);
	ShowWindow(SW_SHOW);
}

void CMonitorViewToolbar::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, ID_BTN_SWITCH_TYPE, m_btnSwitchType);
}

BEGIN_MESSAGE_MAP(CMonitorViewToolbar, CDialogEx)
	ON_BN_CLICKED(ID_BTN_SWITCH_TYPE, &CMonitorViewToolbar::OnBnClickedBtnSwitchType)
	ON_BN_CLICKED(ID_BTN_MEDICAL_RECORD, &CMonitorViewToolbar::OnBnClickedBtnMedicalRecord)
END_MESSAGE_MAP()

// CMonitorViewToolbar 消息处理程序
void CMonitorViewToolbar::OnBnClickedBtnSwitchType()
{
	m_pMonitorView->OnSwitchType();
}

void CMonitorViewToolbar::OnBnClickedBtnMedicalRecord()
{
	m_pMonitorView->OnMedicalRecord();
}
