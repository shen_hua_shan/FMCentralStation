#pragma once

#define OP_SEND     (1)
#define OP_RECV     (0)

class CLogFile;
class CFMRecordUnit;
class CFMSocketUnit
{
friend class CFMSocketThread;

public:
	CFMSocketUnit(int nIndex);
	virtual ~CFMSocketUnit(void);
	void SetSocket(SOCKET s);

private:
	BOOL       m_bValid;
	int        m_nIndex;  //在管理集合中的索引值
	CFMRecordUnit* m_pRecordUnit;
	CCriticalSection m_csOpLock;

	//调试使用的接口
	CTime m_tmCreate;
	//调试使用的接口
	CTime m_tmLastRecv;
	//判断断网超时
	CTime m_tmRecv;

	//与调试相关的函数
	void WriteLogString(CString sFunction, CString sContent);

	//关闭Socket
	void CloseSocket(void);

private:
	DWORD      m_op;
	SOCKET     m_so;
	OVERLAPPED m_ol_send;
	OVERLAPPED m_ol_recv;
	WSABUF     m_wbuf_send;
	WSABUF     m_wbuf_recv;
	char       m_szSendBuf[SOCKET_BUFFER_SIZE];
	char       m_szRecvBuf[SOCKET_BUFFER_SIZE];
	int        m_nRecvLen;
	int        m_nSendLen;
	int        m_nSendTotal;

	//解析用的缓冲区
	BYTE       m_psUserBuf[SOCKET_BUFFER_SIZE];
	int        m_nUserBufLen;

public:
	inline BOOL IsValid() { return m_bValid; };
	inline int GetIndex() { return m_nIndex; };
	inline int GetBedNum() { return (m_nIndex + 1); };
	inline SOCKET GetSocket() { return m_so; };
	inline void SetOpSend() { m_op = OP_SEND; };
	inline void ClrOpSend() { m_op = OP_RECV; };
	inline BOOL OpSend() { return m_op; };
	void Enable(BOOL bEn);
	BOOL CheckSocket(CTime tmNow);

	inline void AddSendLen(int n) {
		m_nSendLen += n;
	};
	inline int GetSendTotal() {
		return m_nSendTotal;
	};
	inline int GetSendLen() {
		return m_nSendLen;
	};
	inline void SetRecvLen(int n) {
		m_nRecvLen = n;
	};

	virtual void OnConnect();
	virtual int  OnReceive(); //返回预期监听的字节数
	virtual void OnSend();

	inline void ResetSendOverlap();
	inline void ResetSendWSABUF();
	inline void ResetRecvOverlap();
	inline void ResetRecvWSABUF();

	inline void SetRecvExpect(int n) {
		m_wbuf_recv.len = n;
	}

	inline OVERLAPPED* GetSendOverlap() {
		return &m_ol_send;
	}
	inline OVERLAPPED* GetRecvOverlap() {
		return &m_ol_recv;
	}
	inline WSABUF* GetSendWSABUF() {
		return &m_wbuf_send;
	}
	inline WSABUF* GetRecvWSABUF() {
		return &m_wbuf_recv;
	}

	BOOL SendData(BYTE* buf, int len);
	BOOL SendSetupECG(
		int ecg_l1, int ecg_l2, int filter, int gain, int stop, int paceangy, int stangy, int rhythm, 
		int hr_h, int hr_l, int st1_h, int st1_l, int st2_h, int st2_l, int pvc_h, int pvc_l);
	BOOL SendSetupNIBP(int sys_h, int sys_l, int mea_h, int mea_l, int dia_h, int dia_l, int nibp_mode);
	BOOL SendSetupRESP(int rr_h, int rr_l);
	BOOL SendSetupSPO2(int spo2_h, int spo2_l, int pr_h, int pr_l);
	BOOL SendSetupTEMP(int t1_h, int t1_l, int t2_h, int t2_l);
	BOOL SendNIBPPumpCmd(int cmd);
	BOOL SendSetupFHR(int fhr_h, int fhr_l);

protected:
	virtual int ReceiveProc(char* buf, int size);//返回预期监听的字节数
	virtual void OnReceiveError(int nErrorNo);
	virtual void OnUserDataReceive(SOCK_PKG* pk);
};
