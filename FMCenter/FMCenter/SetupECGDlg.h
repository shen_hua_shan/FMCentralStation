#pragma once
#include "afxwin.h"


// CSetupECGDlg 对话框
class CFMRecordUnit;
class CSetupECGDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetupECGDlg)

private:
	CFMRecordUnit* m_pru;

public:
	CSetupECGDlg(CWnd* pParent, CFMRecordUnit* pru);
	virtual ~CSetupECGDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_SETUP_ECG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CComboBox m_cmbECG1Lead;
	CComboBox m_cmbECG2Lead;
	CComboBox m_cmbECGFilter;
	CComboBox m_cmbECGGain;
	CComboBox m_cmbECGStop;
	CComboBox m_cmbECGPaceangy;
	CComboBox m_cmbECGStangy;
	CComboBox m_cmbECGRhythm;
	CEdit m_edtHRHigh;
	CEdit m_edtHRLow;
	CEdit m_edtECG1StHigh;
	CEdit m_edtECG1StLow;
	CEdit m_edtECG2StHigh;
	CEdit m_edtECG2StLow;
	CEdit m_edtPVCHigh;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnUpdateEditEcg1StHigh();
	afx_msg void OnUpdateEditEcg1StLow();
};
