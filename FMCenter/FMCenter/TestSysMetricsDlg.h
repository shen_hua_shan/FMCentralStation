#pragma once
#include "afxcmn.h"


// CTestSysMetricsDlg 对话框

class CTestSysMetricsDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CTestSysMetricsDlg)

public:
	CTestSysMetricsDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CTestSysMetricsDlg();

// 对话框数据
	enum { IDD = IDD_SYS_METRICS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_lstSysMetrics;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
};
