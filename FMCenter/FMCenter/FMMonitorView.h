#pragma once
#include "afxwin.h"

class CIniInt;
class CMonitorViewToolbar;
class CFMMonitorView : public CView
{
	DECLARE_DYNCREATE(CFMMonitorView)

public:
	CFMMonitorView(void);
	virtual ~CFMMonitorView(void);

	virtual void OnDraw(CDC* pDC);

friend class CFMMonitorUnit;

protected:
	BOOL m_bInitBkDC;
	CDC* m_arBkDC[MONITOR_BKGD_COUNT];    //存储11张不同规格的格子背景
	CIniInt* m_arBkIni[MONITOR_BKGD_COUNT];  //存储背景图对应的配置信息
	CDC* m_arNumDC[MONITOR_FONT_COUNT];   //存储各种规格的数字图片
	CIniInt* m_arNumIni[MONITOR_FONT_COUNT]; //存储数字图片的配置信息

	CDC* m_pMemDC;       //首先向memDC上绘制，然后再送入屏幕，以避免闪烁
	CDC* m_pDefaultBkDC; //默认背景图，一床都没有时显示此图

	void DrawNumber(CDC* pDC, int x, int y, CString sValue, int nType, int nSubType=0);
	void SetToolbarStatus(int x, int y, CString type);
	void SelectTimeRulerFont();
	void SelectNumberFont();
	void SelectInfoFont();
	void ResetFont();

private:
	//十分之一秒计数器
	int m_nDecimusCounter;

	CMonitorViewToolbar* m_pToolbar;
	//工具条的遮挡区域
	CRgn m_rgnToolBarMask;
	//绘图单元
	CFMMonitorUnit* m_arUnits[MONITOR_UNIT_COUNT];
	//焦点绘图单元
	CFMMonitorUnit* m_puFocus;

	//字体
	CFont m_fontTimeRuler;
	CFont m_fontNumber;
	CFont m_fontInfo;
	CFont* m_pOldFont;

	//加载资源
	void LoadResource();
	void InitMonitorUnit();
	void DrawDefaultBk(CDC* pDC);

	//初始化资源DC
	void InitBkDCandMemDC(CDC* pDC);

	//九种不同的界面布局(包括一个窗口都没有)
	int m_nCurLayoutNum;
	void Layout_0(void);
	void Layout_1(void);
	void Layout_2(void);
	void Layout_3(void);
	void Layout_4(void);
	void Layout_5(void);
	void Layout_6(void);
	void Layout_7(void);
	void Layout_8(void);

public:
	void OnSwitchType();
	void OnMedicalRecord();
	void CloseTimer();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnLayoutChange(WPARAM wp, LPARAM lp);
};

