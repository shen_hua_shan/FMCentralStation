#include "stdafx.h"
#include "ReviewTendWave.h"
#include "ReviewDlg.h"

static const int top_margin = 8;
static const int lft_margin = 100;
static const int rht_margin = 22;
static const int scale_w = 24;
static const int half_scale_h = 6;

static const COLORREF cr_bkgd = RGB(0,0,0);
static const COLORREF cr_normal = RGB(225,225,255);
static const COLORREF cr_grid_light = RGB(220,220,220);
static const COLORREF cr_grid_dark = RGB(205,205,205);
static const COLORREF cr_minite = RGB(192,192,255);
static const COLORREF cr_scale = RGB(0,192,255);
static const COLORREF cr_info = RGB(255,255,225);
static const COLORREF cr_hr   = RGB(0,128,0);
static const COLORREF cr_sys  = RGB(128,0,64);
static const COLORREF cr_mea  = RGB(0,64,128);
static const COLORREF cr_dia  = RGB(0,0,192);
static const COLORREF cr_rr   = RGB(92,92,0);
static const COLORREF cr_spo2 = RGB(128,0,128);
static const COLORREF cr_t1   = RGB(0,128,192);
static const COLORREF cr_t2   = RGB(192,0,0);

static const int hr_max = 350;
static const int hr_grid_y = 5;
static const float hr_scale = 0.5f;

static const int bp_max = 350;
static const int bp_grid_y = 5;
static const float bp_scale = 0.5f;

static const int rr_max = 120;
static const int rr_grid_y = 5;
static const float rr_scale = 1.0f;

static const int tt_max = 50;
static const int tt_grid_y = 5;
static const float tt_scale = 2.0f;

static const int grid_gap_x = 10;
static const int table_gap_y = 20;


CReviewTendWave::CReviewTendWave(CReviewDlg* pdlg)
	: CReviewTool(pdlg)
{
	m_fontInfo.CreateFont(14, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_fontTableScale.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
}

CReviewTendWave::~CReviewTendWave(void)
{
	m_pOldFont = NULL;
}

int CReviewTendWave::GetPageSpan()
{
	CRect rc = m_pReviewDlg->GetTableRect();
	int table_w = rc.Width() - lft_margin - rht_margin;
	return table_w;
}

void CReviewTendWave::EnterReview()
{
	m_pReviewDlg->m_staSelectNibpUnit.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_cmbSelectNibpUnit.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_staSelectTempUnit.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_cmbSelectTempUnit.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_btnSelectParam.ShowWindow(SW_SHOW);
}

void CReviewTendWave::LeaveReview()
{
	m_pReviewDlg->m_staSelectNibpUnit.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_cmbSelectNibpUnit.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_staSelectTempUnit.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_cmbSelectTempUnit.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_btnSelectParam.ShowWindow(SW_HIDE);
}

void CReviewTendWave::Draw(CDC* pdc)
{
	m_pReviewDlg->InitBkgd(pdc, cr_bkgd);

	CRect rc = m_pReviewDlg->GetTableRect();
	rc.OffsetRect(-rc.left, -rc.top);

	m_pOldFont = pdc->SelectObject(&m_fontTableScale);
	pdc->SetBkMode(TRANSPARENT);
	DrawGrid(pdc, rc);
	DrawInfo(pdc, rc);
	DrawTime(pdc, rc);
	DrawData(pdc, rc);
	pdc->SelectObject(m_pOldFont);
}

void CReviewTendWave::DrawGrid(CDC* pdc, CRect& rc)
{
	int i;
	CBrush brTable(cr_normal);

	CPen penRed(PS_SOLID, 1, cr_minite);
	CPen penDark(PS_SOLID, 1, cr_grid_dark);
	CPen penLight(PS_SOLID, 1, cr_grid_light);
	CObject* pOldObj = pdc->SelectObject(&penLight);
	m_pOldFont = pdc->SelectObject(&m_fontTableScale);
	pdc->SetTextColor(cr_scale);
	int table_w = rc.Width() - lft_margin - rht_margin;

	int nNibpUnit = m_pReviewDlg->GetNIBPUnitOpt();
	int nTempUnit = m_pReviewDlg->GetTEMPUnitOpt();

	//心率
	int x = rc.left + lft_margin;
	int y = rc.top + top_margin;
	int h = hr_max * hr_scale;
	CRect rcHR(x, y, x + table_w, y + h);
	pdc->FillRect(&rcHR, &brTable);
	for (i=0; i<table_w; i+= grid_gap_x) {
		if (0 == i % (grid_gap_x * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y + h);
	}
	for (i=0; i<=h; i+= hr_grid_y) {
		if (0 == i % (hr_grid_y * 5)) {
			CString s;
			s.Format(_T("%3d"), (int)(hr_max - i / hr_scale));
			pdc->TextOut(x - scale_w, y - half_scale_h + i, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y + i);
		pdc->LineTo(x + table_w, y + i);
	}

	//血压
	y = y + h + table_gap_y;
	h = bp_max * bp_scale;
	CRect rcBP(x, y, x + table_w, y + h);
	pdc->FillRect(&rcBP, &brTable);
	for (i=0; i<table_w; i+= grid_gap_x) {
		if (0 == i % (grid_gap_x * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y + h);
	}
	for (i=0; i<=h; i+= bp_grid_y) {
		if (0 == i % (bp_grid_y * 5)) {
			CString s = itoNibpString(nNibpUnit, (int)(bp_max - i / bp_scale));
			pdc->TextOut(x - scale_w, y - half_scale_h + i, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y + i);
		pdc->LineTo(x + table_w, y + i);
	}

	//血氧和呼吸率
	y = y + h + table_gap_y;
	h = rr_max * rr_scale;
	CRect rcRR(x, y, x + table_w, y + h);
	pdc->FillRect(&rcRR, &brTable);
	for (i=0; i<table_w; i+= grid_gap_x) {
		if (0 == i % (grid_gap_x * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y + h);
	}
	for (i=0; i<=h; i+= rr_grid_y) {
		if (0 == i % (rr_grid_y * 4)) {
			CString s;
			s.Format(_T("%3d"), (int)(rr_max - i / rr_scale));
			pdc->TextOut(x - scale_w, y - half_scale_h + i, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y + i);
		pdc->LineTo(x + table_w, y + i);
	}

	//体温
	y = y + h + table_gap_y;
	h = tt_max * tt_scale;
	CRect rcTT(x, y, x + table_w, y + h);
	pdc->FillRect(&rcTT, &brTable);
	for (i=0; i<table_w; i+= grid_gap_x) {
		if (0 == i % (grid_gap_x * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y + h);
	}
	for (i=0; i<=h; i+= tt_grid_y) {
		if (0 == i % (tt_grid_y * 4)) {
			CString s = itoTempString(nTempUnit, (int)((tt_max - i / tt_scale) * 10));
			pdc->TextOut(x - scale_w - 6, y - half_scale_h + i, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y + i);
		pdc->LineTo(x + table_w, y + i);
	}

	pdc->SelectObject(m_pOldFont);
	pdc->SelectObject(pOldObj);
}

void CReviewTendWave::DrawInfo(CDC* pdc, CRect& rc)
{
	pdc->SelectObject(&m_fontInfo);
	pdc->SetTextColor(cr_info);

	CString sNibpUnit = CFMDict::NIBPUnit_itos(m_pReviewDlg->GetNIBPUnitOpt());
	CString sTempUnit = CFMDict::TEMPUnit_itos(m_pReviewDlg->GetTEMPUnitOpt());
	CString sItemName;

	int y = rc.top + top_margin;
	int h = hr_max * hr_scale;
	pdc->TextOutW(rc.left + 2, y, _T("心率"));
	y = y + h + table_gap_y;
	h = bp_max * bp_scale;
	sItemName.Format(_T("血压(%s)"), sNibpUnit);
	pdc->TextOutW(rc.left + 2, y, sItemName);
	y = y + h + table_gap_y;
	h = rr_max * rr_scale;
	pdc->TextOutW(rc.left + 2, y, _T("血氧"));
	pdc->TextOutW(rc.left + 2, y + 20, _T("呼吸"));
	y = y + h + table_gap_y;
	h = tt_max * tt_scale;
	sItemName.Format(_T("体温(%s)"), sTempUnit.Right(1));
	pdc->TextOutW(rc.left + 2, y, sItemName);

	//绘制颜色标注
	pdc->SelectObject(&m_fontTableScale);
	y = rc.top + top_margin;
	h = hr_max * hr_scale;
	{
		CBrush br(cr_hr);
		CRect rect(rc.left+10, y+40, rc.left+40, y+52);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 40, _T("HR"));
	y = y + h + table_gap_y;
	h = bp_max * bp_scale;
	{
		CBrush br(cr_sys);
		CRect rect(rc.left+10, y+40, rc.left+40, y+52);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 40, _T("SYS"));
	{
		CBrush br(cr_mea);
		CRect rect(rc.left+10, y+60, rc.left+40, y+72);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 60, _T("MEA"));
	{
		CBrush br(cr_dia);
		CRect rect(rc.left+10, y+80, rc.left+40, y+92);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 80, _T("DIA"));
	y = y + h + table_gap_y;
	h = rr_max * rr_scale;
	{
		CBrush br(cr_spo2);
		CRect rect(rc.left+10, y+60, rc.left+40, y+72);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 60, _T("SPO2"));
	{
		CBrush br(cr_rr);
		CRect rect(rc.left+10, y+80, rc.left+40, y+92);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 80, _T("RR"));
	y = y + h + table_gap_y;
	h = tt_max * tt_scale;
	{
		CBrush br(cr_t1);
		CRect rect(rc.left+10, y+40, rc.left+40, y+52);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 40, _T("T1"));
	{
		CBrush br(cr_t2);
		CRect rect(rc.left+10, y+60, rc.left+40, y+72);
		pdc->FillRect(&rect, &br);
	}
	pdc->TextOutW(rc.left + 50, y + 60, _T("T2"));
	
	pdc->SelectObject(m_pOldFont);
}

void CReviewTendWave::DrawTime(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	if (tmTableStart == 0) {
		return;
	}

	CString sTime = tmTableStart.Format(_T("%Y-%m-%d %H:%M:%S"));
	CString sInfo;
	sInfo.Format(_T("记录时间：%s"), sTime);
	m_pOldFont = pdc->SelectObject(&m_fontTableScale);
	pdc->TextOut(rc.left + 10, rc.bottom - 16, sInfo);

	int tableW = rc.Width() - lft_margin - rht_margin;
	int nDrawLen = min(nValidLen, tableW);
	pdc->SetTextColor(cr_info);
	pdc->SelectObject(&m_fontTableScale);

	int x = rc.left + lft_margin;
	int y = rc.top + top_margin;
	int h = hr_max * hr_scale;
	DrawTimeRuler(pdc, x, y+h, tmTableStart, nDrawLen);
	y = y + h + table_gap_y;
	h = bp_max * bp_scale;
	DrawTimeRuler(pdc, x, y+h, tmTableStart, nDrawLen);
	y = y + h + table_gap_y;
	h = rr_max * rr_scale;
	DrawTimeRuler(pdc, x, y+h, tmTableStart, nDrawLen);
	y = y + h + table_gap_y;
	h = tt_max * tt_scale;
	//由于文字位置遮挡，此标尺向右推2分钟绘制
	CTimeSpan ts(0,0,0,120);
	DrawTimeRuler(pdc, x+120, y+h, tmTableStart+ts, nDrawLen-120);

	pdc->SelectObject(m_pOldFont);
}

void CReviewTendWave::DrawTimeRuler(CDC* pdc, int x, int y, CTime tm, int nSeconds)
{
	int j;
	int count = nSeconds / 120; //两个时间标之间间距2分钟
	
	for (j = 0; j <= count; j ++) {
		int i;
		for (i = 0; i < table_gap_y - 2; i ++) {
			pdc->SetPixel(x, y + i + 1, cr_info);
		}
		CTimeSpan ts(0, 0, 0, j * 120);
		CTime tmMark = tm + ts;
		pdc->TextOut(x + 2, y + 3, tmMark.Format(_T("%H:%M:%S")));
		x += 120;
	}
}

void CReviewTendWave::DrawData(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	int tableW = rc.Width() - lft_margin - rht_margin;
	int nDispCount = min(nValidLen, tableW);

	int i;
	WORD HR[2];
	WORD SYS[2];
	WORD MEA[2];
	WORD DIA[2];
	WORD SPO[2];
	WORD RR[2];
	WORD T1[2];
	WORD T2[2];
	int x = rc.left + lft_margin;
	for (i=0; i<nDispCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		if (0 == i) {
			HR[0]  = CHECK_HR(pe->hr);
			SYS[0] = CHECK_NIBP(pn->sys);
			MEA[0] = CHECK_NIBP(pn->mea);
			DIA[0] = CHECK_NIBP(pn->dia);
			SPO[0] = CHECK_SPO2(ps->spo2);
			RR[0]  = CHECK_RR(pr->rr);
			T1[0]  = CHECK_TEMP(pt->t1);
			T2[0]  = CHECK_TEMP(pt->t2);
		}
		else {
			HR[0]   = HR[1];
			SYS[0]  = SYS[1];
			MEA[0]  = MEA[1];
			DIA[0]  = DIA[1];
			SPO[0] = SPO[1];
			RR[0]   = RR[1];
			T1[0]   = T1[1];
			T2[0]   = T2[1];
		}
		HR[1]  = CHECK_HR(pe->hr);
		SYS[1] = CHECK_NIBP(pn->sys);
		MEA[1] = CHECK_NIBP(pn->mea);
		DIA[1] = CHECK_NIBP(pn->dia);
		SPO[1] = CHECK_SPO2(ps->spo2);
		RR[1]  = CHECK_RR(pr->rr);
		T1[1]  = CHECK_TEMP(pt->t1);
		T2[1]  = CHECK_TEMP(pt->t2);

		int y = rc.top + top_margin;
		int h = hr_max * hr_scale;
		if (INVALID_VALUE != HR[0] && INVALID_VALUE != HR[1]) {
			DrawLine(pdc, cr_hr, x, y + (hr_max - HR[0]) * hr_scale, y + (hr_max - HR[1]) * hr_scale);
		}
		y = y + h + table_gap_y;
		h = bp_max * bp_scale;
		if (INVALID_VALUE != SYS[0] && INVALID_VALUE != SYS[1]) {
			DrawLine(pdc, cr_sys, x, y + (bp_max - SYS[0]) * bp_scale, y + (bp_max - SYS[1]) * bp_scale);
		}
		if (INVALID_VALUE != MEA[0] && INVALID_VALUE != MEA[1]) {
			DrawLine(pdc, cr_mea, x, y + (bp_max - MEA[0]) * bp_scale, y + (bp_max - MEA[1]) * bp_scale);
		}
		if (INVALID_VALUE != DIA[0] && INVALID_VALUE != DIA[1]) {
			DrawLine(pdc, cr_dia, x, y + (bp_max - DIA[0]) * bp_scale, y + (bp_max - DIA[1]) * bp_scale);
		}
		y = y + h + table_gap_y;
		h = rr_max * rr_scale;
		if (INVALID_VALUE != SPO[0] && INVALID_VALUE != SPO[1]) {
			DrawLine(pdc, cr_spo2, x, y + (rr_max - SPO[0]) * rr_scale, y + (rr_max - SPO[1]) * rr_scale);
		}
		if (INVALID_VALUE != RR[0] && INVALID_VALUE != RR[1]) {
			DrawLine(pdc, cr_rr, x, y + (rr_max - RR[0]) * rr_scale, y + (rr_max - RR[1]) * rr_scale);
		}
		y = y + h + table_gap_y;
		if (INVALID_VALUE != T1[0] && INVALID_VALUE != T1[1]) {
			DrawLine(pdc, cr_t1, x, y + (tt_max - T1[0] * 0.1f) * tt_scale, y + (tt_max - T1[1] * 0.1f) * tt_scale);
		}
		if (INVALID_VALUE != T2[0] && INVALID_VALUE != T2[1]) {
			DrawLine(pdc, cr_t2, x, y + (tt_max - T2[0] * 0.1f) * tt_scale, y + (tt_max - T2[1] * 0.1f) * tt_scale);
		}

		x ++;
	}
}
