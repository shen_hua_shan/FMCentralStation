#include "stdafx.h"
#include "FMCenter.h"
#include "PrintTendTable.h"

static const int lft_margin = 200;
static const int top_margin = 150;

static const COLORREF cr_bk_title = RGB(225,225,0);
static const COLORREF cr_bk_alarm = RGB(255,192,192);
static const COLORREF cr_grid = RGB(128,128,0);
static const COLORREF cr_info = RGB(0,0,0);
static const COLORREF cr_text = RGB(0,0,0);
static const COLORREF cr_time = RGB(0,0,0);
static const COLORREF cr_hr   = RGB(0,0,0);
static const COLORREF cr_sys  = RGB(0,0,0);
static const COLORREF cr_mea  = RGB(0,0,0);
static const COLORREF cr_dia  = RGB(0,0,0);
static const COLORREF cr_rr   = RGB(0,0,0);
static const COLORREF cr_spo2 = RGB(0,0,0);
static const COLORREF cr_t1   = RGB(0,0,0);
static const COLORREF cr_t2   = RGB(0,0,0);

static const int grid_column = 9;
static const int grid_line = 41;
static const int grid_w = 277;
static const int grid_h = 42;
static const int page_width = grid_w * grid_column;
static const int page_height = grid_h * grid_line;


CPrintTendTable::CPrintTendTable(CReviewDlg* pdlg)
	: CPrintTool(pdlg)
{
	m_fontEcgInfo.CreateFont(36, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
}

CPrintTendTable::~CPrintTendTable(void)
{
	m_pOldFont = NULL;
}

void CPrintTendTable::SelectEcgInfoFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontEcgInfo);
}

void CPrintTendTable::ResetFont(CDC* pdc)
{
	if (m_pOldFont) {
		pdc->SelectObject(m_pOldFont);
	}
}

void CPrintTendTable::InitSetup(int type)
{
}

void CPrintTendTable::Print(CDC* pdc,  CPrintInfo* pInfo)
{
	//在MM_LOMETRIC映射模式下，Y坐标是反向的
	CRect rc(lft_margin, - top_margin, lft_margin + page_width, - top_margin - page_height);
	SelectEcgInfoFont(pdc);
	pdc->SetBkMode(TRANSPARENT);
	DrawHead(pdc, rc);
	DrawGrid(pdc, rc);
	DrawInfo(pdc, rc);
	DrawData(pdc, rc);
	ResetFont(pdc);
}

void CPrintTendTable::DrawHead(CDC* pdc, CRect& rc)
{
	CRect rcHead(rc.left, rc.top - grid_h, rc.right, rc.top);
	CBrush br(cr_bk_title);
	pdc->FillRect(&rcHead, &br);

	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr_text);

	int x = rc.left + 8;
	int y = rc.top - 3;
	CString sNibpUnit = CFMDict::NIBPUnit_itos(m_pReviewDlg->GetNIBPUnitOpt());
	CString sTempUnit = CFMDict::TEMPUnit_itos(m_pReviewDlg->GetTEMPUnitOpt());

	CString sName;
	pdc->TextOut(x, y, _T("监护时间"));

	x += grid_w;
	pdc->TextOut(x, y, _T("心率"));

	x += grid_w;
	sName.Format(_T("收缩压(%s)"), sNibpUnit);
	pdc->TextOut(x, y, sName);

	x += grid_w;
	sName.Format(_T("平均压(%s)"), sNibpUnit);
	pdc->TextOut(x, y, sName);

	x += grid_w;
	sName.Format(_T("舒张压(%s)"), sNibpUnit);
	pdc->TextOut(x, y, sName);

	x += grid_w;
	pdc->TextOut(x, y, _T("呼吸率"));

	x += grid_w;
	pdc->TextOut(x, y, _T("血氧饱和度"));

	x += grid_w;
	sName.Format(_T("体温1(%s)"), sTempUnit);
	pdc->TextOut(x, y, sName);

	x += grid_w;
	sName.Format(_T("体温2(%s)"), sTempUnit);
	pdc->TextOut(x, y, sName);
}

void CPrintTendTable::DrawGrid(CDC* pdc, CRect& rc)
{
	CPen penDark(PS_SOLID, 1, cr_grid);
	CPen* pOldPen = pdc->SelectObject(&penDark);

	int x, y;
	for (x=rc.left; x<=rc.right; x+= grid_w) {
		pdc->MoveTo(x, rc.top);
		pdc->LineTo(x, rc.bottom);
	}
	for (y=rc.bottom; y<=rc.top; y+= grid_h) {
		pdc->MoveTo(rc.left, y);
		pdc->LineTo(rc.right, y);
	}

	pdc->SelectObject(pOldPen);
}

void CPrintTendTable::DrawInfo(CDC* pdc, CRect& rc)
{
	CString sPatientInfo;
	CString sWaveformInfo;

	CString sBedNum;
	CString sMedNum;
	CString sPatName;
	CString sPatGender;
	CString sPatType;
	CString sPatAge;

	sBedNum.Format(_T("床位：%02s"), m_pReviewDlg->m_sBedNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sBedNum);
	sMedNum.Format(_T("病历号：%s"), m_pReviewDlg->m_sMedicalNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sMedicalNum);
	sPatName.Format(_T("姓名：%s"), m_pReviewDlg->m_sPatientName.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientName);
	sPatGender.Format(_T("性别：%s"), m_pReviewDlg->m_sPatientGender.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientGender);
	sPatType.Format(_T("类型：%s"), m_pReviewDlg->m_sPatientType.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientType);
	sPatAge.Format(_T("年龄：%s"), m_pReviewDlg->m_sPatientType.IsEmpty() ? _T("--") : m_pReviewDlg->m_sPatientAge);

	sPatientInfo.Format(_T("%5s%20s%30s%20s%20s%20s"), sBedNum, sMedNum, sPatName, sPatGender, sPatType, sPatAge);
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr_info);
	pdc->TextOut(rc.left, rc.top + 36, sPatientInfo);

	CString sStartTime;
	CString sWaveformLen;
	CString sPrintTime;
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	sStartTime = tmTableStart.Format(_T("开始时间：%Y-%m-%d %H:%M:%S"));

	int nWaveformLen = min(nValidLen, abs(rc.Height() / grid_h) - 1);
	sWaveformLen.Format(_T("数据长度：%d秒"), nWaveformLen);
	CTime tmNow = CTime::GetCurrentTime();
	sPrintTime = tmNow.Format(_T("打印时间：%Y-%m-%d %H:%M:%S"));

	sWaveformInfo.Format(_T("%20s%30s%68s"), sStartTime, sWaveformLen, sPrintTime);
	pdc->TextOut(rc.left, rc.bottom - 6, sWaveformInfo);
	
	if (! theApp.m_sHospitalName.IsEmpty()) {
		pdc->TextOut(rc.left, rc.bottom - 44, theApp.m_sHospitalName);
	}
}

void CPrintTendTable::DrawData(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	int grid_x = rc.left;
	int grid_y = rc.top - grid_h;
	int text_x_offset = 8;
	int text_y_offset = -3;
	int nibpUnit = m_pReviewDlg->GetNIBPUnitOpt();
	int tempUnit = m_pReviewDlg->GetTEMPUnitOpt();

	int i;
	int nTableSize = min(nValidLen, abs(rc.Height() / grid_h) - 1);
	for (i=0; i<nTableSize; i++) {
		int curX = grid_x;

		CTimeSpan ts(0, 0, 0, i);
		CTime tmCur = tmStart + ts;
		CString sValue = tmCur.Format(_T("%H:%M:%S"));
		pdc->SetTextColor(cr_time);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		if (pe->hr < pe->hr_low || pe->hr > pe->hr_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoNumString3(CHECK_HR(pe->hr));
		pdc->SetTextColor(cr_hr);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		if (pn->sys < pn->sys_low || pn->sys > pn->sys_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoNibpString(nibpUnit, CHECK_NIBP(pn->sys));
		pdc->SetTextColor(cr_sys);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		if (pn->mea < pn->mea_low || pn->mea > pn->mea_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoNibpString(nibpUnit, CHECK_NIBP(pn->mea));
		pdc->SetTextColor(cr_mea);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		if (pn->dia < pn->dia_low || pn->dia > pn->dia_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoNibpString(nibpUnit, CHECK_NIBP(pn->dia));
		pdc->SetTextColor(cr_dia);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		if (pr->rr < pr->rr_low || pr->rr > pr->rr_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoNumString3(CHECK_RR(pr->rr));
		pdc->SetTextColor(cr_rr);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		if (ps->spo2 < ps->spo2_low || ps->spo2 > ps->spo2_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoNumString3(CHECK_SPO2(ps->spo2));
		pdc->SetTextColor(cr_spo2);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		if (pt->t1 < pt->t1_low || pt->t1 > pt->t1_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoTempString(tempUnit, CHECK_TEMP(pt->t1));
		pdc->SetTextColor(cr_t1);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);
		curX += grid_w;

		if (pt->t2 < pt->t2_low || pt->t2 > pt->t2_high) {
			DrawAlarmGrid(pdc, curX, grid_y);
		}
		sValue = itoTempString(tempUnit, CHECK_TEMP(pt->t2));
		pdc->SetTextColor(cr_t2);
		pdc->TextOut(curX + text_x_offset, grid_y + text_y_offset, sValue);

		grid_y -= grid_h;
	}
}

void CPrintTendTable::DrawAlarmGrid(CDC* pdc, int x, int y)
{
	CBrush br(cr_bk_alarm);
	CRect rc(x + 1, y - 1, x + grid_w - 1, y - grid_h + 1);
	pdc->FillRect(&rc, &br);
}