// TestSysMetricsDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "TestSysMetricsDlg.h"
#include "afxdialogex.h"


#define TEST_SYS_METRICS_LIST_COLUMN_COUNT       (3)


//column上面显示的文字
static _TCHAR* l_arColumnLabel[TEST_SYS_METRICS_LIST_COLUMN_COUNT] =
{
	_T("名称"),
	_T("数值"),
	_T("说明")
};
//column上文字的显示方式（靠左）
static int l_arColumnFmt[TEST_SYS_METRICS_LIST_COLUMN_COUNT] = 
{
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
static int l_arColumnWidth[TEST_SYS_METRICS_LIST_COLUMN_COUNT] = 
{
	260,
	100,
	600
};

// CTestSysMetricsDlg 对话框

IMPLEMENT_DYNAMIC(CTestSysMetricsDlg, CDialogEx)

CTestSysMetricsDlg::CTestSysMetricsDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTestSysMetricsDlg::IDD, pParent)
{

}

CTestSysMetricsDlg::~CTestSysMetricsDlg()
{
}

void CTestSysMetricsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_METRICS, m_lstSysMetrics);
}


BEGIN_MESSAGE_MAP(CTestSysMetricsDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CTestSysMetricsDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CTestSysMetricsDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CTestSysMetricsDlg 消息处理程序


void CTestSysMetricsDlg::OnBnClickedOk()
{
	CString info;
	CString sFileName;
	sFileName.Format(_T("%s%s"), theApp.GetDataRoot(), _T("/sys_metrics.txt"));
	CFile file;
	if (! file.Open(sFileName, CFile::modeCreate | CFile::modeWrite)) {
		CString err;
		err.Format(_T("文件创建失败！\n\n请去除%s所在目录的只读属性！\n"), sFileName);
		MessageBox(err);
		exit(0);
	}
	if (2 == sizeof(_TCHAR)) {
		BYTE head[2] = { 0xFF, 0xFE };//UNICODE格式的，写文件头
		file.Write(head, 2);
	}

	CString sContent;
	int i;
	int count = m_lstSysMetrics.GetItemCount();
	for (i=0; i<count; i++) {

		CString sName = m_lstSysMetrics.GetItemText(i, 0);
		CString sValue = m_lstSysMetrics.GetItemText(i, 1);
		CString sNote = m_lstSysMetrics.GetItemText(i, 2);
		
		CString sLine = sName;
		while (sLine.GetLength() < 40) {
			sLine += _T(" ");
		}
		sLine += sValue;
		while (sLine.GetLength() < 60) {
			sLine += _T(" ");
		}
		sLine += sNote;

		sContent += sLine;
		sContent += _T("\r\n");
	}

	file.Write(sContent.GetBuffer(0), sContent.GetLength() * sizeof(_TCHAR));
	file.Close();

	info.Format(_T("文件保存在：%s"), sFileName);
	MessageBox(info);
	CDialogEx::OnOK();
}


void CTestSysMetricsDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}


BOOL CTestSysMetricsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 初始化LIST的表头
	int i;
	m_lstSysMetrics.RemoveAllGroups();
	m_lstSysMetrics.ModifyStyle(0, LVS_REPORT | LVS_SHOWSELALWAYS, 0);
	m_lstSysMetrics.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	//设定一个用于存取column的结构lvc
	LVCOLUMN lvc;
	//设定存取模式
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	//用InSertColumn函数向窗口中插入柱
	for (i=0 ; i<TEST_SYS_METRICS_LIST_COLUMN_COUNT; i++) {
		lvc.iSubItem = i;
		lvc.pszText = l_arColumnLabel[i];
		lvc.cx = l_arColumnWidth[i];
		lvc.fmt = l_arColumnFmt[i];
		m_lstSysMetrics.InsertColumn(i,&lvc);
	}

	//添加列表内容
	i = 0;
	CString s;
	LVITEM lvi;
	_TCHAR buffer[256];
	lvi.mask = LVIF_TEXT;
	lvi.iItem = i;
	lvi.pszText = buffer;
	lvi.cchTextMax = 255;

	s.Format(_T("SM_CLEANBOOT"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_CLEANBOOT));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("0:正常启动；1:安全模式；2:网络安全模式"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXBORDER, SM_CYBORDER"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXBORDER), GetSystemMetrics(SM_CYBORDER));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("Windows窗口边框的高度和宽度。3D形态下等同于SM_CXEDGE参数。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXCURSOR, SM_CYCURSOR"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXCURSOR), GetSystemMetrics(SM_CYCURSOR));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("标准光标的宽度和高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXDLGFRAME, SM_CYDLGFRAME"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXDLGFRAME), GetSystemMetrics(SM_CYDLGFRAME));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("等同于SM_CXFIXEDFRAME和SM_CYFIXEDFRAME。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXDOUBLECLK, SM_CYDOUBLECLK"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXDOUBLECLK), GetSystemMetrics(SM_CYDOUBLECLK));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("双击有效的矩形区域。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXEDGE, SM_CYEDGE"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXEDGE), GetSystemMetrics(SM_CYEDGE));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("3D边框的宽度和高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXFIXEDFRAME, SM_CYFIXEDFRAME"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXFIXEDFRAME), GetSystemMetrics(SM_CYFIXEDFRAME));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("围绕具有标题但无法改变尺寸的窗口（如对话框）的边框厚度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXFRAME, SM_CYFRAME"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXFRAME), GetSystemMetrics(SM_CYFRAME));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("等同于CXSIZEFRAME和CYSIZEFRAME。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXFULLSCREEN, SM_CYFULLSCREEN"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXFULLSCREEN), GetSystemMetrics(SM_CYFULLSCREEN));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("全屏窗口的宽度和高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXHSCROLL, SM_CYHSCROLL"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXHSCROLL), GetSystemMetrics(SM_CYHSCROLL));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("水平滚动条的高度和箭头的宽度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXHTHUMB"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_CXHTHUMB));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("水平滚动条上的滑块宽度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXVSCROLL, SM_CYVSCROLL"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXVSCROLL), GetSystemMetrics(SM_CYVSCROLL));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("垂直滚动条的宽度和垂直滚动条上按钮的高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CYVTHUMB"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_CYVTHUMB));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("垂直滚动条上的滑块高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXICON, SM_CYICON"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXICON), GetSystemMetrics(SM_CYICON));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("系统缺省的图标的高度和宽度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXICONSPACING, SM_CYICONSPACING"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXICONSPACING), GetSystemMetrics(SM_CYICONSPACING));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("以大图标方式查看item时图标之间的间距，这个间距总是大于等于SM_CXICON和SM_CYICON。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXMAXIMIZED, SM_CYMAXIMIZED"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXMAXIMIZED), GetSystemMetrics(SM_CYMAXIMIZED));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("处于顶层的最大化窗口的缺省尺寸。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXMAXTRACK, SM_CYMAXTRACK"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXMAXTRACK), GetSystemMetrics(SM_CYMAXTRACK));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("具有可改变尺寸边框和标题栏的窗口的缺省最大尺寸，如果窗口大于这个尺寸，窗口是不可移动的。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXMENUCHECK, SM_CYMENUCHECK"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXMENUCHECK), GetSystemMetrics(SM_CYMENUCHECK));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("菜单选中标记位图的尺寸。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXMENUSIZE, SM_CYMENUSIZE"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXMENUSIZE), GetSystemMetrics(SM_CXMENUSIZE));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("菜单栏按钮的尺寸。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXMIN, SM_CYMIN"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXMIN), GetSystemMetrics(SM_CYMIN));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("窗口所能达到的最小尺寸。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXMINIMIZED, SM_CYMINIMIZED"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXMINIMIZED), GetSystemMetrics(SM_CYMINIMIZED));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("正常的最小化窗口的尺寸。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXMINTRACK, SM_CYMINTRACK"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXMINTRACK), GetSystemMetrics(SM_CYMINTRACK));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("最小跟踪距离。当使用者拖动窗口移动距离小于这个值，窗口不会移动。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXSCREEN, SM_CYSCREEN"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("屏幕尺寸。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXSIZE, SM_CYSIZE"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXSIZE), GetSystemMetrics(SM_CYSIZE));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("标题栏按钮的尺寸。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXSIZEFRAME, SM_CYSIZEFRAME"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXSIZEFRAME), GetSystemMetrics(SM_CYSIZEFRAME));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("围绕可改变大小的窗口的边框的厚度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CXSMICON, SM_CYSMICON"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d, %d"), GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("小图标的尺寸。小图标一般出现在窗口标题栏上。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CYCAPTION"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_CYCAPTION));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("普通窗口标题的高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CYMENU"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_CYMENU));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("单个菜单条的高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CYSMCAPTION"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_CYSMCAPTION));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("窗口小标题栏的高度。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_DBCSENABLED"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_DBCSENABLED));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("非0表明系统安装了双字节版本的USER.EXE。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_MENUDROPALIGNMENT"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_MENUDROPALIGNMENT));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("非0表明下拉菜单是右对齐，否则是左对齐的。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_MOUSEPRESENT"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_MOUSEPRESENT));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("非0表明安装了鼠标。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_CMOUSEBUTTONS"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_CMOUSEBUTTONS));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("系统支持的鼠标按键数。0表示未安装鼠标。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_MOUSEWHEELPRESENT"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_MOUSEWHEELPRESENT));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("非0表明安装了滚轮鼠标。（Windows NT only）"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	i++;
	lvi.iItem = i;
	s.Format(_T("SM_SWAPBUTTON"));
	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.InsertItem(&lvi);
	s.Format(_T("%d"), GetSystemMetrics(SM_SWAPBUTTON));
	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);
	s.Format(_T("非0表示鼠标左右键交换。"));
	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, s.GetBuffer(0), 255);
	m_lstSysMetrics.SetItem(&lvi);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
