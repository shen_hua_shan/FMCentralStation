// SetupFHRDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SetupFHRDlg.h"
#include "afxdialogex.h"
#include "FMSocketThread.h"
#include "FMSocketUnit.h"
#include "FMRecordUnit.h"

// CSetupFHRDlg 对话框

IMPLEMENT_DYNAMIC(CSetupFHRDlg, CDialogEx)

CSetupFHRDlg::CSetupFHRDlg(CWnd* pParent, CFMRecordUnit* pru, int nPaperSpeedOpt)
	: CDialogEx(CSetupFHRDlg::IDD, pParent)
	, m_pru(pru)
{
	m_nFHRPaterSpeedOpt = nPaperSpeedOpt;
}

CSetupFHRDlg::~CSetupFHRDlg()
{
}

void CSetupFHRDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FHR_HIGH, m_edtFHRHigh);
	DDX_Control(pDX, IDC_EDIT_FHR_LOW, m_edtFHRLow);
	DDX_Control(pDX, IDC_COMBO_PAPER_SPEED, m_cmbPaperSpeed);
	DDX_Control(pDX, IDC_EDT_PREGNANT_WEEK, m_edtPregnantWeek);
}

BEGIN_MESSAGE_MAP(CSetupFHRDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSetupFHRDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSetupFHRDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_SEND, &CSetupFHRDlg::OnBnClickedSend)
	ON_BN_CLICKED(IDC_BTN_SAVE_PREGNANT_WEEK, &CSetupFHRDlg::OnBnClickedBtnSavePregnantWeek)
END_MESSAGE_MAP()

// CSetupFHRDlg 消息处理程序
void CSetupFHRDlg::OnBnClickedSend()
{
	// 发送设置
	CString sValue;
	m_edtFHRHigh.GetWindowText(sValue);
	int fhr_h = _ttoi(sValue);
	m_edtFHRLow.GetWindowText(sValue);
	int fhr_l = _ttoi(sValue);

	if (fhr_h < 50 || fhr_h > 210) {
		MessageBox(_T("胎心率上限超出范围！"));
		m_edtFHRHigh.SetFocus();
		return;
	}
	if (fhr_l < 50 || fhr_l > 210) {
		MessageBox(_T("胎心率下限超出范围！"));
		m_edtFHRLow.SetFocus();
		return;
	}
	if (fhr_l >= fhr_h) {
		MessageBox(_T("胎心率下限不能超越上限设定！"));
		m_edtFHRLow.SetFocus();
		return;
	}

	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	psu->SendSetupFHR(fhr_h, fhr_l);
}

void CSetupFHRDlg::OnBnClickedOk()
{
	m_nFHRPaterSpeedOpt = m_cmbPaperSpeed.GetCurSel();
	CDialogEx::OnOK();
}

void CSetupFHRDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

BOOL CSetupFHRDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_cmbPaperSpeed.ResetContent();
	int i;
	int count = CFMDict::GetFHRPaperSpeedOptCount();
	for (i = 0; i < count; i ++) {
		m_cmbPaperSpeed.AddString(CFMDict::FHRPaperSpeedOpt_itos(i));
	}
	m_cmbPaperSpeed.SetCurSel(m_nFHRPaterSpeedOpt);
	m_edtPregnantWeek.SetWindowText(m_pru->GetPregnantWeek());
	m_edtPregnantWeek.SetLimitText(10);

	DISPLAY_DATA* pdd = m_pru->GetLatestNumbData();
	if (pdd) {
		CString sValue;
		sValue.Format(_T("%d"), pdd->fhr_h);
		m_edtFHRHigh.SetWindowText(sValue);
		sValue.Format(_T("%d"), pdd->fhr_l);
		m_edtFHRLow.SetWindowText(sValue);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CSetupFHRDlg::OnBnClickedBtnSavePregnantWeek()
{
	CString sPregWeek;
	m_edtPregnantWeek.GetWindowText(sPregWeek);
	if (sPregWeek != m_pru->GetPregnantWeek()) {
		CString sMRID = m_pru->GetRecordID();
		MonitorRecordSetPregnantWeek(sMRID, sPregWeek);
		m_pru->SetPregnantWeek(sPregWeek);
	}
}

void CSetupFHRDlg::MonitorRecordSetPregnantWeek(CString& sMRID, CString& sPregnantWeek)
{
	if (theApp.DebugUI()) {
		return;
	}

	CString sql;
	sql.Format(
		_T("UPDATE monitor_record SET pregnant_week='%s',operator='%s' ")
		_T("WHERE mrid='%s'"),
		sPregnantWeek, theApp.m_sAdminName, sMRID);

	//执行SQL语句
	CSmartLock lock(theApp.m_csDBLock);
	try {
		theApp.m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CSetupFHRDlg::MonitorRecordSetPregnantWeek第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
	}
}