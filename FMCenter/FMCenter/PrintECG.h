#pragma once
#include "reviewdlg.h"
class CPrintECG : public CPrintTool
{
public:
	CPrintECG(CReviewDlg* pdlg);
	virtual ~CPrintECG(void);

public:
	virtual void InitSetup(int type=0);
	virtual void Print(CDC* pdc,  CPrintInfo* pInfo);

private:
	CFont m_fontEcgInfo;
	CFont* m_pOldFont;

private:
	void SelectEcgInfoFont(CDC* pdc);
	void ResetFont(CDC* pdc);
	void DrawGrid(CDC* pdc, CRect& rc);
	void DrawInfo(CDC* pdc, CRect& rc);
	void DrawWave(CDC* pdc, CRect& rc);
	void DrawLeadInfo(CDC* pdc, int x, int y, int h, int lead);
};

