﻿// FM_MACRO.h

#ifndef __FM_MACRO_H__
#define __FM_MACRO_H__

// 定义屏幕绘制曲线的模式
#define DECIMUS_VERSION
//每1/10秒绘制一次曲线；
//如果关闭此模式，则每1秒绘制一次曲线，曲线明显有跳动感


// 定义界面外观
#define CLIENT_W                       (1920)
#define CLIENT_H                       (1080)
#define MAIN_AREA_W                    (1910)
#define MAIN_AREA_H                    (1012)
#define MAIN_TOOLBAR_W                 (MAIN_AREA_W)
#define MAIN_TOOLBAR_H                 (55)
#define PAGE_COUNT                     (8)    //总的页数
#define UNIT_PER_PAGE                  (8)    //每页管理的单元数量

#define MAIN_WINDOW_TITLE              (_T("中央监护站"))

// 默认的病历号起始值（第一个病历号是该数值加1），注意这是一个全数字字符串
#define DEFAULT_MEDICAL_NUM_START      (_T("111100"))
#define DEFAULT_RECORD_ID_START        (_T("10000"))
//

// 定义监护类型
#define MONITOR_TYPE_ADULT             (1)
#define MONITOR_TYPE_FETUS             (2)
#define MONITOR_TYPE_BOTH              (3)
// 定义显示类型
#define MONITOR_AREA_1                 (1)
#define MONITOR_AREA_2                 (2)
#define MONITOR_AREA_3                 (3)
#define MONITOR_AREA_4                 (4)
#define MONITOR_AREA_5                 (5)
//

//定义编辑类型
#define TYPE_OP_NORMAL                 (1)
#define TYPE_OP_SHOW                   (2)
#define TYPE_OP_EDIT                   (3)
#define TYPE_OP_NEW                    (4)
#define TYPE_OP_SELECT                 (5)
#define TYPE_OP_SIM_ADD                (6)
#define TYPE_OP_SIM_DEL                (7)
//

//定义边框宽度
#define FRAME_WIDTH_FOCUS              (3)
#define FRAME_WIDTH_NORMAL             (1)

//数据库名称
#define FMDB_DBNAME                    (_T("FMServerDB"))
//
#define CLIENT_COUNT                   (PAGE_COUNT * UNIT_PER_PAGE)

//定义配置文件文件名
#define CONFIG_FILE                    (_T("config.ini"))
//定义记录文件存放路径
#define RECORD_DIR                     (_T("/RECORD"))
//日志文件
#define LOG_FILE                       (_T("/SYSTEM/log.txt"))
//

//背景颜色，当采用色阶法绘制曲线时，需要使用背景颜色
#define COLOR_WAVE_BK                  (RGB(0,0,0))
//

#define MONITOR_UNIT_COUNT             (8)
#define MONITOR_BKGD_COUNT             (11)  //背景图数量
#define MONITOR_FONT_COUNT             (22)  //字体数量

//背景资源索引
#define INDEX_A1                       (0)
#define INDEX_A2                       (1)
#define INDEX_A3                       (2)
#define INDEX_A4                       (3)
#define INDEX_A5                       (4)
#define INDEX_F1                       (5)
#define INDEX_F2                       (6)
#define INDEX_F3                       (7)
#define INDEX_F4                       (8)
#define INDEX_F5                       (9)
#define INDEX_FA                       (10)
//成人监护界面的背景图
#define BMP_A1                         (_T("a1.bmp"))
#define INI_A1                         (_T("a1.ini"))
#define BMP_A2                         (_T("a2.bmp"))
#define INI_A2                         (_T("a2.ini"))
#define BMP_A3                         (_T("a3.bmp"))
#define INI_A3                         (_T("a3.ini"))
#define BMP_A4                         (_T("a4.bmp"))
#define INI_A4                         (_T("a4.ini"))
#define BMP_A5                         (_T("a5.bmp"))
#define INI_A5                         (_T("a5.ini"))
//纯胎儿监护的背景图
#define BMP_F1                         (_T("f1.bmp"))
#define INI_F1                         (_T("f1.ini"))
#define BMP_F2                         (_T("f2.bmp"))
#define INI_F2                         (_T("f2.ini"))
#define BMP_F3                         (_T("f3.bmp"))
#define INI_F3                         (_T("f3.ini"))
#define BMP_F4                         (_T("f4.bmp"))
#define INI_F4                         (_T("f4.ini"))
#define BMP_F5                         (_T("f5.bmp"))
#define INI_F5                         (_T("f5.ini"))
//母亲胎儿监护的背景图
#define BMP_FA                         (_T("fa.bmp"))
#define INI_FA                         (_T("fa.ini"))
//各种颜色、大小的数字
#define BMP_FONT_1                     (_T("num1.bmp"))
#define INI_FONT_1                     (_T("num1.ini"))
#define BMP_FONT_2                     (_T("num2.bmp"))
#define INI_FONT_2                     (_T("num2.ini"))
#define BMP_FONT_3                     (_T("num3.bmp"))
#define INI_FONT_3                     (_T("num3.ini"))
#define BMP_FONT_4                     (_T("num4.bmp"))
#define INI_FONT_4                     (_T("num4.ini"))
#define BMP_FONT_5                     (_T("num5.bmp"))
#define INI_FONT_5                     (_T("num5.ini"))
#define BMP_FONT_6                     (_T("num6.bmp"))
#define INI_FONT_6                     (_T("num6.ini"))
#define BMP_FONT_7                     (_T("num7.bmp"))
#define INI_FONT_7                     (_T("num7.ini"))
#define BMP_FONT_8                     (_T("num8.bmp"))
#define INI_FONT_8                     (_T("num8.ini"))
#define BMP_FONT_9                     (_T("num9.bmp"))
#define INI_FONT_9                     (_T("num9.ini"))
#define BMP_FONT_10                    (_T("num10.bmp"))
#define INI_FONT_10                    (_T("num10.ini"))
#define BMP_FONT_11                    (_T("num11.bmp"))
#define INI_FONT_11                    (_T("num11.ini"))
#define BMP_FONT_12                    (_T("num12.bmp"))
#define INI_FONT_12                    (_T("num12.ini"))
#define BMP_FONT_13                    (_T("num13.bmp"))
#define INI_FONT_13                    (_T("num13.ini"))
#define BMP_FONT_14                    (_T("num14.bmp"))
#define INI_FONT_14                    (_T("num14.ini"))
#define BMP_FONT_15                    (_T("num15.bmp"))
#define INI_FONT_15                    (_T("num15.ini"))
#define BMP_FONT_16                    (_T("num16.bmp"))
#define INI_FONT_16                    (_T("num16.ini"))
#define BMP_FONT_17                    (_T("num17.bmp"))
#define INI_FONT_17                    (_T("num17.ini"))
#define BMP_FONT_18                    (_T("num18.bmp"))
#define INI_FONT_18                    (_T("num18.ini"))
#define BMP_FONT_19                    (_T("num19.bmp"))
#define INI_FONT_19                    (_T("num19.ini"))
#define BMP_FONT_20                    (_T("num20.bmp"))
#define INI_FONT_20                    (_T("num20.ini"))
#define BMP_FONT_21                    (_T("num21.bmp"))
#define INI_FONT_21                    (_T("num21.ini"))
#define BMP_FONT_22                    (_T("num22.bmp"))
#define INI_FONT_22                    (_T("num22.ini"))
//

//各种定时器ID
#define REDRAW_TIMER_ID                (101)

#ifdef DECIMUS_VERSION
  #define REDRAW_TIMER_SPAN              (100)
#else
  #define REDRAW_TIMER_SPAN              (1000)
#endif

#define RENEW_DATETIME_ID              (102)
#define RENEW_DATETIME_SPAN            (1000)
//

//定义FHR走纸速度
#define FHR_PAPER_SPEED_3CM            (0)
#define FHR_PAPER_SPEED_1CM            (1)
//

//定义ECG的显示比例
#define REVIEW_ZOOM_SCALE10            (1)
#define REVIEW_ZOOM_SCALE20            (2)
#define REVIEW_ZOOM_SCALE50            (3)
#define REVIEW_ZOOM_SCALE100           (4)
//

//与监护窗口小工具条相关的内容
#define TOOLBAR_H                      (33)
#define TOOLBAR_W_START                (190)
#define TOOLBAR_W_PAUSE                (400)
#define TOOLBAR_TEXT_FULL_SCREEN       (_T("全屏"))
#define TOOLBAR_TEXT_WINDOW            (_T("窗口"))
#define TOOLBAR_TEXT_ADULT             (_T("成人"))
#define TOOLBAR_TEXT_FETUS             (_T("胎儿"))
#define TOOLBAR_TEXT_BOTH              (_T("综合"))
#define TOOLBAR_TEXT_START             (_T("监护"))
#define TOOLBAR_TEXT_PAUSE             (_T("查看"))
//
#define TEXT_HR_TOO_HIGH               (_T("HR过高"))
#define TEXT_HR_TOO_LOW                (_T("HR过低"))
#define TEXT_ST1_TOO_HIGH              (_T("ST1过高"))
#define TEXT_ST1_TOO_LOW               (_T("ST1过低"))
#define TEXT_ST2_TOO_HIGH              (_T("ST2过高"))
#define TEXT_ST2_TOO_LOW               (_T("ST2过低"))
#define TEXT_PVC_TOO_HIGH              (_T("PVC过高"))
#define TEXT_RR_TOO_HIGH               (_T("RR过高"))
#define TEXT_RR_TOO_LOW                (_T("RR过低"))
#define TEXT_PR_TOO_HIGH               (_T("PR过高"))
#define TEXT_PR_TOO_LOW                (_T("PR过低"))
#define TEXT_SPO2_TOO_HIGH             (_T("SPO2过高"))
#define TEXT_SPO2_TOO_LOW              (_T("SPO2过低"))
#define TEXT_SYS_TOO_HIGH              (_T("SYS过高"))
#define TEXT_SYS_TOO_LOW               (_T("SYS过低"))
#define TEXT_MEA_TOO_HIGH              (_T("MAP过高"))
#define TEXT_MEA_TOO_LOW               (_T("MAP过低"))
#define TEXT_DIA_TOO_HIGH              (_T("DIA过高"))
#define TEXT_DIA_TOO_LOW               (_T("DIA过低"))
#define TEXT_T1_TOO_HIGH               (_T("T1过高"))
#define TEXT_T1_TOO_LOW                (_T("T1过低"))
#define TEXT_T2_TOO_HIGH               (_T("T2过高"))
#define TEXT_T2_TOO_LOW                (_T("T2过低"))
#define TEXT_FHR1_TOO_HIGH             (_T("FHR1过高"))
#define TEXT_FHR1_TOO_LOW              (_T("FHR1过低"))
#define TEXT_FHR2_TOO_HIGH             (_T("FHR2过高"))
#define TEXT_FHR2_TOO_LOW              (_T("FHR2过低"))
#define TEXT_ALARM_UNKNOWN             (_T("未知类型的报警信息"))
//

//两路FHR监护曲线之间的差值
#define FHR1_FHR2_WAVE_GAP             (40)
//

//网络协议
//1．床边机IP地址：192.168.2.(10+X)（X为床号，范围1---64）
//本地端口地址：6000 + X （X为床号，范围1---64）
//2．中央站IP地址：192.168.2.1，监听端口：5000
//#define SERVER_ADDR                    (_T("192.168.1.20"))
//#define SERVER_ADDR                    (_T("localhost"))
//#define SERVER_PORT                    (5000)//改从配置文件读取2014-07-24
#define CLIENT_ADDR_BASE               (10)
#define CLIENT_PORT                    (6000)
#define TIMER_SPAN                     (1000)//客户端每1秒钟发一组数据

//SOCKET工作参数
#define SOCKET_WAIT_TIMEOUT            (100)
//

//自定义通知消息
#define WM_USER_SOCKET_CHANGE          (WM_USER + 100)
#define WM_USER_LAYOUT_CHANGE          (WM_USER + 101)
#define WM_USER_INIT_BK_FINISH         (WM_USER + 102)
//

#endif // __FM_MACRO_H__
