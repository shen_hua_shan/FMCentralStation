//Midi.h		用于播放背景音乐和WAV文件的类
//
//////////////////////////////////////////////////////////////////////

#ifndef __MIDI_H__
#define __MIDI_H__

#include "mmsystem.h"
#pragma comment(lib,"winmm.lib")

#define MIDI_EVENT_QUIT     1
#define MIDI_EVENT_PLAY     2
#define MIDI_EVENT_REPLAY   3
#define MIDI_EVENT_STOP     4

class CMidi  
{
public:
	CMidi();
	virtual ~CMidi();

public:
	void  Init(HWND hWnd);

    void  PlayMidi(WCHAR* FileName);
    void  ReplayMidi(WPARAM w);
    void  StopMidi();
	void  PlayWave(WCHAR* FileName);

    DWORD DoPlayMidi();
	void  DoReplayMidi();
	void  DoStopMidi();

	BOOL   m_bRun;     // 控制PlayMidi线程运行的变量
	HANDLE m_hEvent;   // 通知PlayMidi线程的事件
	int    m_nEvent;   // 事件类型
//
private:
	WCHAR*              m_psMidiFile;
	HWND				m_hWnd;
    UINT				m_wDeviceID;//MCI装置代号
    DWORD				m_dwReturn;
    MCI_OPEN_PARMS		m_mciOpenParms;
    MCI_PLAY_PARMS		m_mciPlayParms;
    MCI_STATUS_PARMS	m_mciStatusParms;
    MCI_SEQ_SET_PARMS	m_mciSeqSetParms;
};

#endif // __MIDI_H__
