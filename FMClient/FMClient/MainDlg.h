#pragma once

#include "resource.h"
#include "FM_STRUCTS.h"
// CMainDlg 对话框

class CClientSocket;
class CMainDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMainDlg)

private:
	BOOL m_bRunning;
	CClientSocket* m_pSocket;

public:
	int  GetBedNum(void);
	BOOL SendData(BYTE* buffer, int length);
	void OnSocketClose(int nErrorCode);
	void OnSocketAccept(int nErrorCode);
	void OnSocketConnect(int nErrorCode);
	void OnSocketReceiveError(int nErrorCode);
	void OnSocketReceive(SOCK_PKG* pDataPkg);

public:
	CMainDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CMainDlg();

// 对话框数据
	enum { IDD = IDD_FMCLIENT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
