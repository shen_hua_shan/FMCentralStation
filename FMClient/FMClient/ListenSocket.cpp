// FMListenSocket.cpp : ʵ���ļ�
//

#include "stdafx.h"
#include "ListenSocket.h"
#include "MainDlg.h"

// CFMListenSocket
IMPLEMENT_DYNAMIC(CListenSocket, CAsyncSocket)
CListenSocket::CListenSocket(CMainDlg* pu)
{
	m_pu = pu;
}

CListenSocket::~CListenSocket()
{
	m_pu = NULL;
}

void CListenSocket::OnAccept(int nErrorCode)
{
	if (m_pu) {
		m_pu->OnSocketAccept(nErrorCode);
	}
	CAsyncSocket::OnAccept(nErrorCode);
}
